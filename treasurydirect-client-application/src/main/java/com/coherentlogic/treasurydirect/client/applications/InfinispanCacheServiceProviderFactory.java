package com.coherentlogic.treasurydirect.client.applications;

import org.infinispan.manager.EmbeddedCacheManager;

import com.coherentlogic.infinispan.cache.providers.DefaultInfinispanCacheServiceProviderFactory;

public class InfinispanCacheServiceProviderFactory extends DefaultInfinispanCacheServiceProviderFactory {

    public static final String DEFAULT_TREASURY_DIRECT_CACHE_NAME = "treasuryDirectCache";

    public InfinispanCacheServiceProviderFactory(EmbeddedCacheManager cacheManager) {
        this (cacheManager, DEFAULT_TREASURY_DIRECT_CACHE_NAME);
    }

    public InfinispanCacheServiceProviderFactory(
        EmbeddedCacheManager cacheManager,
        String defaultCacheName
    ) {
        super(cacheManager, defaultCacheName);
    }
}
