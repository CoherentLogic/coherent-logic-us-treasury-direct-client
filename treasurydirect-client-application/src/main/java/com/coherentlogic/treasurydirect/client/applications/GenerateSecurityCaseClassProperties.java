package com.coherentlogic.treasurydirect.client.applications;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;

import com.coherentlogic.treasurydirect.client.core.domain.Security;

public class GenerateSecurityCaseClassProperties {

    public static void main(String[] args) {

        for (Field field : Security.class.getDeclaredFields()) {

            if (! (Modifier.isStatic(field.getModifiers()) && java.lang.reflect.Modifier.isFinal(field.getModifiers()))) {

//                if ("java.lang.String".equals(field.getType().getName())) {
//                    System.out.println(field.getName() + " : Option[String],");
//                } else if ("java.math.BigDecimal".equals(field.getType().getName())) {
//                    System.out.println(field.getName() + " : Option[BigDecimal],");
//                } else if ("java.math.BigInteger".equals(field.getType().getName())) {
//                    System.out.println(field.getName() + " : Option[BigInteger],");
//                } else if ("java.lang.Boolean".equals(field.getType().getName())) {
//                    System.out.println(field.getName() + " : Option[Boolean],");
//                } else if ("java.lang.Long".equals(field.getType().getName())) {
//                    System.out.println(field.getName() + " : Option[Long],");
//                } else if ("java.lang.Integer".equals(field.getType().getName())) {
//                    System.out.println(field.getName() + " : Option[Integer],");
//                } else if ("java.util.Date".equals(field.getType().getName())) {
//                    System.out.println(field.getName() + " : Option[Date],");
//                }

                String fieldName = field.getName();

                fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1) + " ()";

                if (BigDecimal.class.equals(field.getType()))
                    System.out.println("if (security.get" + fieldName + " == null) { null } else { Some(new scala.math.BigDecimal( security.get" + fieldName + ")) },");
                else if (BigInteger.class.equals(field.getType()))
                    System.out.println("if (security.get" + fieldName + " == null) { null } else { Some(new scala.math.BigInteger( security.get" + fieldName + ")) },");
                else if (java.util.Date.class.equals (field.getType())) {
                    System.out.println("Some(toSQLDate (security.get" + fieldName + ")),");
                } else
                    System.out.println("Some(security.get" + fieldName + "),");
            }
        }
    }
}
