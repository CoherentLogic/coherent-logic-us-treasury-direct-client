package com.coherentlogic.treasurydirect.client.applications;

import org.infinispan.Cache;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.transaction.LockingMode;
import org.infinispan.transaction.TransactionMode;
import org.infinispan.transaction.lookup.EmbeddedTransactionManagerLookup;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import com.coherentlogic.coherent.data.adapter.core.cache.MapCompliantCacheServiceProvider;
import com.coherentlogic.treasurydirect.client.core.builders.QueryBuilder;
import com.coherentlogic.treasurydirect.client.core.extractors.SecuritiesExtractor;

@org.springframework.context.annotation.Configuration
public class ApplicationGlobalConfiguration {

	public static final String TREASURY_DIRECT_QUERY_BUILDER = "treasuryDirectQueryBuilder";

//    public static final String DISTRIBUTED_CACHE_SERVICE_PROVIDER = "cacheServiceProvider",
//        TREASURY_DIRECT_CACHE_MANAGER = "treasuryDirectCacheManager";
//
//    @Bean(name=TREASURY_DIRECT_CACHE_MANAGER)
//    public EmbeddedCacheManager getDefaultCacheManager () {
//
//        GlobalConfigurationBuilder globalConfigurationBuilder = GlobalConfigurationBuilder.defaultClusteredBuilder();
//
//        GlobalConfiguration globalConfiguration =
//            globalConfigurationBuilder
//                .transport()
//                .addProperty("configurationFile", "infinispan-default-jgroups-tcp.xml")
//                .clusterName("TreasuryDirectClientCluster")
//                .defaultTransport()
//                .defaultCacheName("TreasuryDirectClientCache")
//                .build();
//
//        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder ();
//
//        Configuration configuration = configurationBuilder
//            .transaction()
//                .transactionMode(TransactionMode.TRANSACTIONAL)
//                .lockingMode(LockingMode.PESSIMISTIC)
//                .autoCommit(false)
//                 // https://docs.jboss.org/infinispan/9.0/apidocs/org/infinispan/transaction/lookup/TransactionManagerLookup.html
//                 // EmbeddedTransactionManagerLookup
//                .transactionManagerLookup(new EmbeddedTransactionManagerLookup ())
//            .jmxStatistics()
//            .clustering()
//               .cacheMode(CacheMode.DIST_SYNC)
//               .l1()
//                .lifespan(60000L * 5L) // Five minutes
//            .build();
//
//        DefaultCacheManager result = new DefaultCacheManager(globalConfiguration, configuration);
//
//        return result;
//    }
    
	
	
	
//	<!-- init-method="start" destroy-method="stop" -->
//    <bean id="infinispanCacheServiceProviderFactory"
//     class="com.coherentlogic.fred.client.webstart.application.InfinispanCacheServiceProviderFactory">
//         <!-- These two args are instantiated in the JavaConfiguration class. -->
//         <constructor-arg ref="#{T(com.coherentlogic.fred.client.webstart.application.ApplicationConfiguration).FRED_CACHE_MANAGER}"/>
//         <constructor-arg name = "defaultCacheName">
//             <util:constant static-field="com.coherentlogic.fred.client.webstart.application.ApplicationConfiguration.DEFAULT_CACHE_NAME"/>
//         </constructor-arg>
//    </bean>
	
	
	
	
	
	
//        <bean id="queryBuilderFactory" class="com.coherentlogic.fred.client.core.factories.QueryBuilderFactory">
//    <constructor-arg name="restTemplate" ref="fredRestTemplate"/>
//    <constructor-arg name="uri" value="https://api.stlouisfed.org/fred/"/>
//    <constructor-arg name="cacheServiceProvider" ref="fredCacheServiceProvider"/>
//    <!-- NOTE: You must supply your own API key to run this example.
//      -->
//    <constructor-arg name="apiKey" ref="fredApiKey"/>
//</bean>

//	
//	
//	@Bean(name=TREASURY_DIRECT_QUERY_BUILDER)
//	public QueryBuilder getQueryBuilder (RestTemplate restTemplate, ) {
//		return new QueryBuilder (restTemplate, );
//	}
//    
    
    
    
    
//
//    @Bean(name=DISTRIBUTED_CACHE_ENABLED_QUERY_BUILDER)
//    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//    public QueryBuilder getDistributedCacheEnabledQueryBuilder (
//        @Qualifier(DISTRIBUTED_CACHE_SERVICE_PROVIDER) MapCompliantCacheServiceProvider<String>
//            cacheServiceProvider
//    ) {
//    	
//    	EmbeddedCacheManager.
//    	
//        return new QueryBuilder (restTemplate, new MapCompliantCacheServiceProvider(cache), securitiesExtractor, debtsExtractor);
//    }
//
//    @Bean(name=DISTRIBUTED_CACHE_ENABLED_QUERY_BUILDER)
//    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//    public MapCompliantCacheServiceProvider<String, > getDefaultMapCompliantCacheServiceProvider (
//        @Qualifier(TREASURY_DIRECT_CACHE_MANAGER) EmbeddedCacheManager embeddedCacheManager
//    ) {
//        Cache<String, > cache = embeddedCacheManager.getCache();
//
//        return new MapCompliantCacheServiceProvider(cache);
//    }
}
