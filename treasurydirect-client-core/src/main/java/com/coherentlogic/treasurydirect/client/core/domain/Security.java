package com.coherentlogic.treasurydirect.client.core.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Representation of individual security data returned from calls to the treasurydirect.gov web services.
 *
 * @TODO: Fix all boolean methods so that isWhatever is the getter and not getWhatever.
 *
 * @see https://www.treasurydirect.gov/instit/research/gloss/gloss.htm
 * @see https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
 * @see https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Security extends SerializableBean {

    private static final long serialVersionUID = 6384300918412594365L;

    /**
     * "cusip":"912796CJ6"
     */
    private String cusip;

    /**
     * Getter method for the cusip property.
     */
    public String getCusip () {
        return cusip;
    }

    public static final String CUSIP = "cusip";

    /**
     * Setter method for the cusip property.
     */
    public void setCusip (@Changeable(CUSIP) String cusip) {
        this.cusip = cusip;
    }

    /**
     * "issueDate":"2014-02-11T00:00:00"
     */
    private Date issueDate;

    /**
     * Getter method for the issueDate property.
     */
    public Date getIssueDate () {
        return issueDate;
    }

    public static final String ISSUEDATE = "issueDate";

    /**
     * Setter method for the issueDate property.
     */
    public void setIssueDate (@Changeable(ISSUEDATE) Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * "securityType":"Note"
     * "securityType":"Bond"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * @TODO: Use an enum.
     */
    private String securityType;

    /**
     * Getter method for the securityType property.
     */
    public String getSecurityType () {
        return securityType;
    }

    public static final String SECURITYTYPE = "securityType";

    /**
     * Setter method for the securityType property.
     */
    public void setSecurityType (@Changeable(SECURITYTYPE) String securityType) {
        this.securityType = securityType;
    }

    /**
     * "securityTerm":"4-Year 4-Month"
     * "securityTerm":"30-Year"
     * "securityTerm":"9-Year 10-Month"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * @TODO: Use an enum.
     */
    private String securityTerm;

    /**
     * Getter method for the securityTerm property.
     */
    public String getSecurityTerm () {
        return securityTerm;
    }

    public static final String SECURITYTERM = "securityTerm";

    /**
     * Setter method for the securityTerm property.
     */
    public void setSecurityTerm (@Changeable(SECURITYTERM) String securityTerm) {
        this.securityTerm = securityTerm;
    }

    /**
     * "maturityDate":"2028-01-15T00:00:00"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     */
    private Date maturityDate;

    /**
     * Getter method for the maturityDate property.
     */
    public Date getMaturityDate () {
        return maturityDate;
    }

    public static final String MATURITYDATE = "maturityDate";

    /**
     * Setter method for the maturityDate property.
     */
    public void setMaturityDate (@Changeable(MATURITYDATE) Date maturityDate) {
        this.maturityDate = maturityDate;
    }

    /**
     * 0.125000
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private BigDecimal interestRate;

    /**
     * Getter method for the interestRate property.
     */
    public BigDecimal getInterestRate () {
        return interestRate;
    }

    public static final String INTERESTRATE = "interestRate";

    /**
     * Setter method for the interestRate property.
     */
    public void setInterestRate (@Changeable(INTERESTRATE) BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    /**
     * 246.678100
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private BigDecimal refCpiOnIssueDate;

    /**
     * Getter method for the refCpiOnIssueDate property.
     */
    public BigDecimal getRefCpiOnIssueDate () {
        return refCpiOnIssueDate;
    }

    public static final String REFCPIONISSUEDATE = "refCpiOnIssueDate";

    /**
     * Setter method for the refCpiOnIssueDate property.
     */
    public void setRefCpiOnIssueDate (@Changeable(REFCPIONISSUEDATE) BigDecimal refCpiOnIssueDate) {
        this.refCpiOnIssueDate = refCpiOnIssueDate;
    }

    /**
     * 243.195530
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private BigDecimal refCpiOnDatedDate;

    /**
     * Getter method for the refCpiOnDatedDate property.
     */
    public BigDecimal getRefCpiOnDatedDate () {
        return refCpiOnDatedDate;
    }

    public static final String REFCPIONDATEDDATE = "refCpiOnDatedDate";

    /**
     * Setter method for the refCpiOnDatedDate property.
     */
    public void setRefCpiOnDatedDate (@Changeable(REFCPIONDATEDDATE) BigDecimal refCpiOnDatedDate) {
        this.refCpiOnDatedDate = refCpiOnDatedDate;
    }

    /**
     * 2017-12-14T00:00:00
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private Date announcementDate;

    /**
     * Getter method for the announcementDate property.
     */
    public Date getAnnouncementDate () {
        return announcementDate;
    }

    public static final String ANNOUNCEMENTDATE = "announcementDate";

    /**
     * Setter method for the announcementDate property.
     */
    public void setAnnouncementDate (@Changeable(ANNOUNCEMENTDATE) Date announcementDate) {
        this.announcementDate = announcementDate;
    }

    /**
     * 2017-12-21T00:00:00
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private Date auctionDate;

    /**
     * Getter method for the auctionDate property.
     */
    public Date getAuctionDate () {
        return auctionDate;
    }

    public static final String AUCTIONDATE = "auctionDate";

    /**
     * Setter method for the auctionDate property.
     */
    public void setAuctionDate (@Changeable(AUCTIONDATE) Date auctionDate) {
        this.auctionDate = auctionDate;
    }

    /**
     * 2017
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     *
     * @TODO: Should this be of type Integer?
     */
    private String auctionDateYear;

    /**
     * Getter method for the auctionDateYear property.
     */
    public String getAuctionDateYear () {
        return auctionDateYear;
    }

    public static final String AUCTIONDATEYEAR = "auctionDateYear";

    /**
     * Setter method for the auctionDateYear property.
     */
    public void setAuctionDateYear (@Changeable(AUCTIONDATEYEAR) String auctionDateYear) {
        this.auctionDateYear = auctionDateYear;
    }

    /**
     * 2017-10-15T00:00:00
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private Date datedDate;

    /**
     * Getter method for the datedDate property.
     */
    public Date getDatedDate () {
        return datedDate;
    }

    public static final String DATEDDATE = "datedDate";

    /**
     * Setter method for the datedDate property.
     */
    public void setDatedDate (@Changeable(DATEDDATE) Date datedDate) {
        this.datedDate = datedDate;
    }

    /**
     * @see adjustedAccruedInterestPer1000
     *
     * @TODO: No non-null examples available however this is likely a BigDecimal.
     */
    private BigDecimal accruedInterestPer1000;

    /**
     * Getter method for the accruedInterestPer1000 property.
     */
    public BigDecimal getAccruedInterestPer1000 () {
        return accruedInterestPer1000;
    }

    public static final String ACCRUEDINTERESTPER1000 = "accruedInterestPer1000";

    /**
     * Setter method for the accruedInterestPer1000 property.
     */
    public void setAccruedInterestPer1000 (@Changeable(ACCRUEDINTERESTPER1000) BigDecimal accruedInterestPer1000) {
        this.accruedInterestPer1000 = accruedInterestPer1000;
    }

    /**
     * @see adjustedAccruedInterestPer1000
     *
     * @TODO: No non-null examples available however this is likely a BigDecimal.
     */
    private BigDecimal accruedInterestPer100;

    /**
     * Getter method for the accruedInterestPer100 property.
     */
    public BigDecimal getAccruedInterestPer100 () {
        return accruedInterestPer100;
    }

    public static final String ACCRUEDINTERESTPER100 = "accruedInterestPer100";

    /**
     * Setter method for the accruedInterestPer100 property.
     */
    public void setAccruedInterestPer100 (@Changeable(ACCRUEDINTERESTPER100) BigDecimal accruedInterestPer100) {
        this.accruedInterestPer100 = accruedInterestPer100;
    }

    /**
     * 0.2567300000
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private BigDecimal adjustedAccruedInterestPer1000;

    /**
     * Getter method for the adjustedAccruedInterestPer1000 property.
     */
    public BigDecimal getAdjustedAccruedInterestPer1000 () {
        return adjustedAccruedInterestPer1000;
    }

    public static final String ADJUSTEDACCRUEDINTERESTPER1000 = "adjustedAccruedInterestPer1000";

    /**
     * Setter method for the adjustedAccruedInterestPer1000 property.
     */
    public void setAdjustedAccruedInterestPer1000 (@Changeable(ADJUSTEDACCRUEDINTERESTPER1000) BigDecimal adjustedAccruedInterestPer1000) {
        this.adjustedAccruedInterestPer1000 = adjustedAccruedInterestPer1000;
    }

    /**
     * 100.1966110000
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private BigDecimal adjustedPrice;

    /**
     * Getter method for the adjustedPrice property.
     */
    public BigDecimal getAdjustedPrice () {
        return adjustedPrice;
    }

    public static final String ADJUSTEDPRICE = "adjustedPrice";

    /**
     * Setter method for the adjustedPrice property.
     */
    public void setAdjustedPrice (@Changeable(ADJUSTEDPRICE) BigDecimal adjustedPrice) {
        this.adjustedPrice = adjustedPrice;
    }

    /**
     * 24.490000
     * 94.400000
     * 42.760000
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private BigDecimal allocationPercentage;

    /**
     * Getter method for the allocationPercentage property.
     */
    public BigDecimal getAllocationPercentage () {
        return allocationPercentage;
    }

    public static final String ALLOCATIONPERCENTAGE = "allocationPercentage";

    /**
     * Setter method for the allocationPercentage property.
     */
    public void setAllocationPercentage (@Changeable(ALLOCATIONPERCENTAGE) BigDecimal allocationPercentage) {
        this.allocationPercentage = allocationPercentage;
    }

    /**
     * 2
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     *
     * @TODO: A Short may be more appropriate for this property.
     */
    private Integer allocationPercentageDecimals;

    /**
     * Getter method for the allocationPercentageDecimals property.
     */
    public Integer getAllocationPercentageDecimals () {
        return allocationPercentageDecimals;
    }

    public static final String ALLOCATIONPERCENTAGEDECIMALS = "allocationPercentageDecimals";

    /**
     * Setter method for the allocationPercentageDecimals property.
     */
    public void setAllocationPercentageDecimals (@Changeable(ALLOCATIONPERCENTAGEDECIMALS) Integer allocationPercentageDecimals) {
        this.allocationPercentageDecimals = allocationPercentageDecimals;
    }

    /**
     * @TODO: No non-null examples available at the moment however as this is a CUSIP, it will remain as a String.
     */
    private String announcedCusip;

    /**
     * Getter method for the announcedCusip property.
     */
    public String getAnnouncedCusip () {
        return announcedCusip;
    }

    public static final String ANNOUNCEDCUSIP = "announcedCusip";

    /**
     * Setter method for the announcedCusip property.
     */
    public void setAnnouncedCusip (@Changeable(ANNOUNCEDCUSIP) String announcedCusip) {
        this.announcedCusip = announcedCusip;
    }

    /**
     * Single-Price
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     *
     * @TODO: Can this be an enum?
     */
    private String auctionFormat;

    /**
     * Getter method for the auctionFormat property.
     */
    public String getAuctionFormat () {
        return auctionFormat;
    }

    public static final String AUCTIONFORMAT = "auctionFormat";

    /**
     * Setter method for the auctionFormat property.
     */
    public void setAuctionFormat (@Changeable(AUCTIONFORMAT) String auctionFormat) {
        this.auctionFormat = auctionFormat;
    }

    /**
     * "averageMedianDiscountRate":"0.000000"
     *
     * https://www.treasurydirect.gov/webapis/webapisecurities.htm
     */
    private BigDecimal averageMedianDiscountRate;

    /**
     * Getter method for the averageMedianDiscountRate property.
     */
    public BigDecimal getAverageMedianDiscountRate () {
        return averageMedianDiscountRate;
    }

    public static final String AVERAGEMEDIANDISCOUNTRATE = "averageMedianDiscountRate";

    /**
     * Setter method for the averageMedianDiscountRate property.
     */
    public void setAverageMedianDiscountRate (@Changeable(AVERAGEMEDIANDISCOUNTRATE) BigDecimal averageMedianDiscountRate) {
        this.averageMedianDiscountRate = averageMedianDiscountRate;
    }

    /**
     * @TODO: No non-null examples available at the moment.
     *
     * @see averageMedianDiscountRate
     */
    private BigDecimal averageMedianInvestmentRate;

    /**
     * Getter method for the averageMedianInvestmentRate property.
     */
    public BigDecimal getAverageMedianInvestmentRate () {
        return averageMedianInvestmentRate;
    }

    public static final String AVERAGEMEDIANINVESTMENTRATE = "averageMedianInvestmentRate";

    /**
     * Setter method for the averageMedianInvestmentRate property.
     */
    public void setAverageMedianInvestmentRate (@Changeable(AVERAGEMEDIANINVESTMENTRATE) BigDecimal averageMedianInvestmentRate) {
        this.averageMedianInvestmentRate = averageMedianInvestmentRate;
    }

    /**
     * @TODO: No non-null examples available at the moment.
     */
    private BigDecimal averageMedianPrice;

    /**
     * Getter method for the averageMedianPrice property.
     */
    public BigDecimal getAverageMedianPrice () {
        return averageMedianPrice;
    }

    public static final String AVERAGEMEDIANPRICE = "averageMedianPrice";

    /**
     * Setter method for the averageMedianPrice property.
     */
    public void setAverageMedianPrice (@Changeable(AVERAGEMEDIANPRICE) BigDecimal averageMedianPrice) {
        this.averageMedianPrice = averageMedianPrice;
    }

    /**
     * 0.028000
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=FRN&days=720&callback=?
     */
    private BigDecimal averageMedianDiscountMargin;

    /**
     * Getter method for the averageMedianDiscountMargin property.
     */
    public BigDecimal getAverageMedianDiscountMargin () {
        return averageMedianDiscountMargin;
    }

    public static final String AVERAGEMEDIANDISCOUNTMARGIN = "averageMedianDiscountMargin";

    /**
     * Setter method for the averageMedianDiscountMargin property.
     */
    public void setAverageMedianDiscountMargin (@Changeable(AVERAGEMEDIANDISCOUNTMARGIN) BigDecimal averageMedianDiscountMargin) {
        this.averageMedianDiscountMargin = averageMedianDiscountMargin;
    }

    /**
     * "averageMedianYield":"0.345000"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     */
    private BigDecimal averageMedianYield;

    /**
     * Getter method for the averageMedianYield property.
     */
    public BigDecimal getAverageMedianYield () {
        return averageMedianYield;
    }

    public static final String AVERAGEMEDIANYIELD = "averageMedianYield";

    /**
     * Setter method for the averageMedianYield property.
     */
    public void setAverageMedianYield (@Changeable(AVERAGEMEDIANYIELD) BigDecimal averageMedianYield) {
        this.averageMedianYield = averageMedianYield;
    }

    /**
     * "backDated":"Yes"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     */
    private Boolean backDated;

    /**
     * Getter method for the backDated property.
     */
    public Boolean getBackDated () {
        return backDated;
    }

    public static final String BACKDATED = "backDated";

    /**
     * Setter method for the backDated property.
     */
    public void setBackDated (@Changeable(BACKDATED) Boolean backDated) {
        this.backDated = backDated;
    }

    /**
     * "backDatedDate":"2017-10-15T00:00:00"
     * "backDatedDate":"2018-01-15T00:00:00"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     */
    private Date backDatedDate;

    /**
     * Getter method for the backDatedDate property.
     */
    public Date getBackDatedDate () {
        return backDatedDate;
    }

    public static final String BACKDATEDDATE = "backDatedDate";

    /**
     * Setter method for the backDatedDate property.
     */
    public void setBackDatedDate (@Changeable(BACKDATEDDATE) Date backDatedDate) {
        this.backDatedDate = backDatedDate;
    }

    /**
     * "bidToCoverRatio":"2.780000"
     * "bidToCoverRatio":"2.560000"
     * "bidToCoverRatio":"2.310000"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     */
    private BigDecimal bidToCoverRatio;

    /**
     * Getter method for the bidToCoverRatio property.
     */
    public BigDecimal getBidToCoverRatio () {
        return bidToCoverRatio;
    }

    public static final String BIDTOCOVERRATIO = "bidToCoverRatio";

    /**
     * Setter method for the bidToCoverRatio property.
     */
    public void setBidToCoverRatio (@Changeable(BIDTOCOVERRATIO) BigDecimal bidToCoverRatio) {
        this.bidToCoverRatio = bidToCoverRatio;
    }

    /**
     * @TODO No example available for this at the moment so leaving this as a String.
     */
    private String callDate;

    /**
     * Getter method for the callDate property.
     */
    public String getCallDate () {
        return callDate;
    }

    public static final String CALLDATE = "callDate";

    /**
     * Setter method for the callDate property.
     */
    public void setCallDate (@Changeable(CALLDATE) String callDate) {
        this.callDate = callDate;
    }

    /**
     * No
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private Boolean callable;

    /**
     * Getter method for the callable property.
     */
    public Boolean getCallable () {
        return callable;
    }

    public static final String CALLABLE = "callable";

    /**
     * Setter method for the callable property.
     */
    public void setCallable (@Changeable(CALLABLE) Boolean callable) {
        this.callable = callable;
    }

    /**
     * @TODO No example available for this at the moment so leaving this as a String.
     */
    private String calledDate;

    /**
     * Getter method for the calledDate property.
     */
    public String getCalledDate () {
        return calledDate;
    }

    public static final String CALLEDDATE = "calledDate";

    /**
     * Setter method for the calledDate property.
     */
    public void setCalledDate (@Changeable(CALLEDDATE) String calledDate) {
        this.calledDate = calledDate;
    }

    /**
     * No
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private Boolean cashManagementBillCMB;

    /**
     * Getter method for the cashManagementBillCMB property.
     */
    public Boolean getCashManagementBillCMB () {
        return cashManagementBillCMB;
    }

    public static final String CASHMANAGEMENTBILLCMB = "cashManagementBillCMB";

    /**
     * Setter method for the cashManagementBillCMB property.
     */
    public void setCashManagementBillCMB (@Changeable(CASHMANAGEMENTBILLCMB) Boolean cashManagementBillCMB) {
        this.cashManagementBillCMB = cashManagementBillCMB;
    }

    /**
     * 01:00 PM
     */
    private Date closingTimeCompetitive;

    /**
     * Getter method for the closingTimeCompetitive property.
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    public Date getClosingTimeCompetitive () {
        return closingTimeCompetitive;
    }

    public static final String CLOSINGTIMECOMPETITIVE = "closingTimeCompetitive";

    /**
     * Setter method for the closingTimeCompetitive property.
     */
    public void setClosingTimeCompetitive (@Changeable(CLOSINGTIMECOMPETITIVE) Date closingTimeCompetitive) {
        this.closingTimeCompetitive = closingTimeCompetitive;
    }

    /**
     * 	11:00 AM
     *  12:00 PM
     * 
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     */
    private Date closingTimeNoncompetitive;

    /**
     * Getter method for the closingTimeNoncompetitive property.
     */
    public Date getClosingTimeNoncompetitive () {
        return closingTimeNoncompetitive;
    }

    public static final String CLOSINGTIMENONCOMPETITIVE = "closingTimeNoncompetitive";

    /**
     * Setter method for the closingTimeNoncompetitive property.
     */
    public void setClosingTimeNoncompetitive (@Changeable(CLOSINGTIMENONCOMPETITIVE) Date closingTimeNoncompetitive) {
        this.closingTimeNoncompetitive = closingTimeNoncompetitive;
    }

    /**
     * "competitiveAccepted":"13957084000"
     * "competitiveAccepted":"10983900000"
     * "competitiveAccepted":"6976330200"
     */
    private Long competitiveAccepted;

    /**
     * Getter method for the competitiveAccepted property.
     */
    public Long getCompetitiveAccepted () {
        return competitiveAccepted;
    }

    public static final String COMPETITIVEACCEPTED = "competitiveAccepted";

    /**
     * Setter method for the competitiveAccepted property.
     */
    public void setCompetitiveAccepted (@Changeable(COMPETITIVEACCEPTED) Long competitiveAccepted) {
        this.competitiveAccepted = competitiveAccepted;
    }

    /**
     * "competitiveBidDecimals":"3"
     */
    private Integer competitiveBidDecimals;

    /**
     * Getter method for the competitiveBidDecimals property.
     */
    public Integer getCompetitiveBidDecimals () {
        return competitiveBidDecimals;
    }

    public static final String COMPETITIVEBIDDECIMALS = "competitiveBidDecimals";

    /**
     * Setter method for the competitiveBidDecimals property.
     */
    public void setCompetitiveBidDecimals (@Changeable(COMPETITIVEBIDDECIMALS) Integer competitiveBidDecimals) {
        this.competitiveBidDecimals = competitiveBidDecimals;
    }

    /**
     * "competitiveTendered":"38922325000"
     * "competitiveTendered":"28102880000"
     * "competitiveTendered":"16123113400"
     *
     * TODO: Long or BigDecimal?
     */
    private Long competitiveTendered;

    /**
     * Getter method for the competitiveTendered property.
     */
    public Long getCompetitiveTendered () {
        return competitiveTendered;
    }

    public static final String COMPETITIVETENDERED = "competitiveTendered";

    /**
     * Setter method for the competitiveTendered property.
     */
    public void setCompetitiveTendered (@Changeable(COMPETITIVETENDERED) Long competitiveTendered) {
        this.competitiveTendered = competitiveTendered;
    }

    /**
     * "competitiveTendersAccepted":"Yes"
     */
    private Boolean competitiveTendersAccepted;

    /**
     * Getter method for the competitiveTendersAccepted property.
     */
    public Boolean getCompetitiveTendersAccepted () {
        return competitiveTendersAccepted;
    }

    public static final String COMPETITIVETENDERSACCEPTED = "competitiveTendersAccepted";

    /**
     * Setter method for the competitiveTendersAccepted property.
     */
    public void setCompetitiveTendersAccepted (@Changeable(COMPETITIVETENDERSACCEPTED) Boolean competitiveTendersAccepted) {
        this.competitiveTendersAccepted = competitiveTendersAccepted;
    }

    /**
     * "corpusCusip":"9128206Y8"
     * "corpusCusip":"9128204G9"
     * "corpusCusip":"912803FC2"
     */
    private String corpusCusip;

    /**
     * Getter method for the corpusCusip property.
     */
    public String getCorpusCusip () {
        return corpusCusip;
    }

    public static final String CORPUSCUSIP = "corpusCusip";

    /**
     * Setter method for the corpusCusip property.
     */
    public void setCorpusCusip (@Changeable(CORPUSCUSIP) String corpusCusip) {
        this.corpusCusip = corpusCusip;
    }

    /**
     * "cpiBaseReferencePeriod":"1982-1984=100"
     */
    private String cpiBaseReferencePeriod;

    /**
     * Getter method for the cpiBaseReferencePeriod property.
     */
    public String getCpiBaseReferencePeriod () {
        return cpiBaseReferencePeriod;
    }

    public static final String CPIBASEREFERENCEPERIOD = "cpiBaseReferencePeriod";

    /**
     * Setter method for the cpiBaseReferencePeriod property.
     */
    public void setCpiBaseReferencePeriod (@Changeable(CPIBASEREFERENCEPERIOD) String cpiBaseReferencePeriod) {
        this.cpiBaseReferencePeriod = cpiBaseReferencePeriod;
    }

    /**
     * "currentlyOutstanding":"30435000000.000000"
     * "currentlyOutstanding":"14911000000.000000"
     * "currentlyOutstanding":"" (yes "" is correct)
     */
    private String currentlyOutstanding;

    /**
     * Getter method for the currentlyOutstanding property.
     */
    public String getCurrentlyOutstanding () {
        return currentlyOutstanding;
    }

    public static final String CURRENTLYOUTSTANDING = "currentlyOutstanding";

    /**
     * Setter method for the currentlyOutstanding property.
     */
    public void setCurrentlyOutstanding (@Changeable(CURRENTLYOUTSTANDING) String currentlyOutstanding) {
        this.currentlyOutstanding = currentlyOutstanding;
    }

    /**
     * "directBidderAccepted":"1833000000"
     * "directBidderAccepted":"1707000000"
     * "directBidderAccepted":"268000000"
     */
    private Long directBidderAccepted;

    /**
     * Getter method for the directBidderAccepted property.
     */
    public Long getDirectBidderAccepted () {
        return directBidderAccepted;
    }

    public static final String DIRECTBIDDERACCEPTED = "directBidderAccepted";

    /**
     * Setter method for the directBidderAccepted property.
     */
    public void setDirectBidderAccepted (@Changeable(DIRECTBIDDERACCEPTED) Long directBidderAccepted) {
        this.directBidderAccepted = directBidderAccepted;
    }

    /**
     * "directBidderTendered":"2268000000"
     * "directBidderTendered":"2158000000"
     * "directBidderTendered":"683000000"
     */
    private Long directBidderTendered;

    /**
     * Getter method for the directBidderTendered property.
     */
    public Long getDirectBidderTendered () {
        return directBidderTendered;
    }

    public static final String DIRECTBIDDERTENDERED = "directBidderTendered";

    /**
     * Setter method for the directBidderTendered property.
     */
    public void setDirectBidderTendered (@Changeable(DIRECTBIDDERTENDERED) Long directBidderTendered) {
        this.directBidderTendered = directBidderTendered;
    }

    /**
     * "estimatedAmountOfPubliclyHeldMaturingSecuritiesByType":"0"
     * "estimatedAmountOfPubliclyHeldMaturingSecuritiesByType":"68245000000"
     */
    private String estimatedAmountOfPubliclyHeldMaturingSecuritiesByType;

    /**
     * Getter method for the estimatedAmountOfPubliclyHeldMaturingSecuritiesByType property.
     */
    public String getEstimatedAmountOfPubliclyHeldMaturingSecuritiesByType () {
        return estimatedAmountOfPubliclyHeldMaturingSecuritiesByType;
    }

    public static final String ESTIMATEDAMOUNTOFPUBLICLYHELDMATURINGSECURITIESBYTYPE = "estimatedAmountOfPubliclyHeldMaturingSecuritiesByType";

    /**
     * Setter method for the estimatedAmountOfPubliclyHeldMaturingSecuritiesByType property.
     */
    public void setEstimatedAmountOfPubliclyHeldMaturingSecuritiesByType (@Changeable(ESTIMATEDAMOUNTOFPUBLICLYHELDMATURINGSECURITIESBYTYPE) String estimatedAmountOfPubliclyHeldMaturingSecuritiesByType) {
        this.estimatedAmountOfPubliclyHeldMaturingSecuritiesByType = estimatedAmountOfPubliclyHeldMaturingSecuritiesByType;
    }

    /**
     * "fimaIncluded":"Yes"
     */
    private Boolean fimaIncluded;

    /**
     * Getter method for the fimaIncluded property.
     */
    public Boolean getFimaIncluded () {
        return fimaIncluded;
    }

    public static final String FIMAINCLUDED = "fimaIncluded";

    /**
     * Setter method for the fimaIncluded property.
     */
    public void setFimaIncluded (@Changeable(FIMAINCLUDED) Boolean fimaIncluded) {
        this.fimaIncluded = fimaIncluded;
    }

    /**
     * https://www.treasurydirect.gov/instit/annceresult/press/preanre/2017/R_20170926_2.pdf
     *
     * "noncompetitiveAccepted":"4960000"
     * "noncompetitiveAccepted":"42919400"
     * "noncompetitiveAccepted":"16102700"
     * "noncompetitiveAccepted":"23681800"
     *
     * This is money-related so I've set the type to BigDecimal.
     */
    private BigDecimal fimaNoncompetitiveAccepted;

    /**
     * Getter method for the fimaNoncompetitiveAccepted property.
     */
    public BigDecimal getFimaNoncompetitiveAccepted () {
        return fimaNoncompetitiveAccepted;
    }

    public static final String FIMANONCOMPETITIVEACCEPTED = "fimaNoncompetitiveAccepted";

    /**
     * Setter method for the fimaNoncompetitiveAccepted property.
     */
    public void setFimaNoncompetitiveAccepted (@Changeable(FIMANONCOMPETITIVEACCEPTED) BigDecimal fimaNoncompetitiveAccepted) {
        this.fimaNoncompetitiveAccepted = fimaNoncompetitiveAccepted;
    }

    /**
     * "fimaNoncompetitiveTendered":"0"
     *
     * https://www.treasurydirect.gov/instit/annceresult/press/preanre/2017/R_20170926_2.pdf
     */
    private BigDecimal fimaNoncompetitiveTendered;

    /**
     * Getter method for the fimaNoncompetitiveTendered property.
     */
    public BigDecimal getFimaNoncompetitiveTendered () {
        return fimaNoncompetitiveTendered;
    }

    public static final String FIMANONCOMPETITIVETENDERED = "fimaNoncompetitiveTendered";

    /**
     * Setter method for the fimaNoncompetitiveTendered property.
     */
    public void setFimaNoncompetitiveTendered (@Changeable(FIMANONCOMPETITIVETENDERED) BigDecimal fimaNoncompetitiveTendered) {
        this.fimaNoncompetitiveTendered = fimaNoncompetitiveTendered;
    }

    /**
     * "firstInterestPeriod":"Normal"
     *
     * @TODO Use an enum?
     */
    private String firstInterestPeriod;

    /**
     * Getter method for the firstInterestPeriod property.
     */
    public String getFirstInterestPeriod () {
        return firstInterestPeriod;
    }

    public static final String FIRSTINTERESTPERIOD = "firstInterestPeriod";

    /**
     * Setter method for the firstInterestPeriod property.
     */
    public void setFirstInterestPeriod (@Changeable(FIRSTINTERESTPERIOD) String firstInterestPeriod) {
        this.firstInterestPeriod = firstInterestPeriod;
    }

    /**
     * TBD
     */
    private Date firstInterestPaymentDate;

    /**
     * Getter method for the firstInterestPaymentDate property.
     */
    public Date getFirstInterestPaymentDate () {
        return firstInterestPaymentDate;
    }

    public static final String FIRSTINTERESTPAYMENTDATE = "firstInterestPaymentDate";

    /**
     * Setter method for the firstInterestPaymentDate property.
     */
    public void setFirstInterestPaymentDate (@Changeable(FIRSTINTERESTPAYMENTDATE) Date firstInterestPaymentDate) {
        this.firstInterestPaymentDate = firstInterestPaymentDate;
    }

    /**
     * "floatingRate":"No"
     */
    private String floatingRate;

    /**
     * Getter method for the floatingRate property.
     */
    public String getFloatingRate () {
        return floatingRate;
    }

    public static final String FLOATINGRATE = "floatingRate";

    /**
     * Setter method for the floatingRate property.
     */
    public void setFloatingRate (@Changeable(FLOATINGRATE) String floatingRate) {
        this.floatingRate = floatingRate;
    }

    /**
     * @TODO
     */
    private Date frnIndexDeterminationDate;

    /**
     * Getter method for the frnIndexDeterminationDate property.
     */
    public Date getFrnIndexDeterminationDate () {
        return frnIndexDeterminationDate;
    }

    public static final String FRNINDEXDETERMINATIONDATE = "frnIndexDeterminationDate";

    /**
     * Setter method for the frnIndexDeterminationDate property.
     */
    public void setFrnIndexDeterminationDate (@Changeable(FRNINDEXDETERMINATIONDATE) Date frnIndexDeterminationDate) {
        this.frnIndexDeterminationDate = frnIndexDeterminationDate;
    }

    /**
     * @TODO: Need a non-null example, should probably be a BigDecimal.
     */
    private BigDecimal frnIndexDeterminationRate;

    /**
     * Getter method for the frnIndexDeterminationRate property.
     */
    public BigDecimal getFrnIndexDeterminationRate () {
        return frnIndexDeterminationRate;
    }

    public static final String FRNINDEXDETERMINATIONRATE = "frnIndexDeterminationRate";

    /**
     * Setter method for the frnIndexDeterminationRate property.
     */
    public void setFrnIndexDeterminationRate (@Changeable(FRNINDEXDETERMINATIONRATE) BigDecimal frnIndexDeterminationRate) {
        this.frnIndexDeterminationRate = frnIndexDeterminationRate;
    }

    /**
     * @TODO: Need a non-null example, should probably be a BigDecimal.
     *
     * @see https://www.treasurydirect.gov/instit/marketables/tbills/tbills.htm
     */
    private BigDecimal highDiscountRate;

    /**
     * Getter method for the highDiscountRate property.
     */
    public BigDecimal getHighDiscountRate () {
        return highDiscountRate;
    }

    public static final String HIGHDISCOUNTRATE = "highDiscountRate";

    /**
     * Setter method for the highDiscountRate property.
     */
    public void setHighDiscountRate (@Changeable(HIGHDISCOUNTRATE) BigDecimal highDiscountRate) {
        this.highDiscountRate = highDiscountRate;
    }

    /**
     * @see #highDiscountRate
     *
     * @TODO: Need a non-null example, should probably be a BigDecimal.
     */
    private BigDecimal highInvestmentRate;

    /**
     * Getter method for the highInvestmentRate property.
     */
    public BigDecimal getHighInvestmentRate () {
        return highInvestmentRate;
    }

    public static final String HIGHINVESTMENTRATE = "highInvestmentRate";

    /**
     * Setter method for the highInvestmentRate property.
     */
    public void setHighInvestmentRate (@Changeable(HIGHINVESTMENTRATE) BigDecimal highInvestmentRate) {
        this.highInvestmentRate = highInvestmentRate;
    }

    /**
     * "highPrice":"100.374263"
     * "highPrice":"97.935177"
     * "highPrice":"99.895379"
     */
    private String highPrice;

    /**
     * Getter method for the highPrice property.
     */
    public String getHighPrice () {
        return highPrice;
    }

    public static final String HIGHPRICE = "highPrice";

    /**
     * Setter method for the highPrice property.
     */
    public void setHighPrice (@Changeable(HIGHPRICE) String highPrice) {
        this.highPrice = highPrice;
    }

    /**
     *
     * @see <a href="https://www.treasurydirect.gov/instit/statreg/auctreg/FRNTermSheet.pdf">TREASURY FLOATING RATE NOTE TERM SHEET</a>
     *
     * @see https://www.investopedia.com/terms/d/discountmargin.asp
     *
     * @TODO: Type is TBD as no non-null examples exist at the moment.
     */
    private BigDecimal highDiscountMargin;

    /**
     * Getter method for the highDiscountMargin property.
     */
    public BigDecimal getHighDiscountMargin () {
        return highDiscountMargin;
    }

    public static final String HIGHDISCOUNTMARGIN = "highDiscountMargin";

    /**
     * Setter method for the highDiscountMargin property.
     */
    public void setHighDiscountMargin (@Changeable(HIGHDISCOUNTMARGIN) BigDecimal highDiscountMargin) {
        this.highDiscountMargin = highDiscountMargin;
    }

    /**
     * "highYield":"0.3700"
     * "highYield":"0.7640"
     * "highYield":"1.0030"
     */
    private BigDecimal highYield;

    /**
     * Getter method for the highYield property.
     */
    public BigDecimal getHighYield () {
        return highYield;
    }

    public static final String HIGHYIELD = "highYield";

    /**
     * Setter method for the highYield property.
     */
    public void setHighYield (@Changeable(HIGHYIELD) BigDecimal highYield) {
        this.highYield = highYield;
    }

    /**
     * "indexRatioOnIssueDate":"1.014320"
     * "indexRatioOnIssueDate":"1.004340"
     * "indexRatioOnIssueDate":"0.999730"
     */
    private BigDecimal indexRatioOnIssueDate;

    /**
     * Getter method for the indexRatioOnIssueDate property.
     */
    public BigDecimal getIndexRatioOnIssueDate () {
        return indexRatioOnIssueDate;
    }

    public static final String INDEXRATIOONISSUEDATE = "indexRatioOnIssueDate";

    /**
     * Setter method for the indexRatioOnIssueDate property.
     */
    public void setIndexRatioOnIssueDate (@Changeable(INDEXRATIOONISSUEDATE) BigDecimal indexRatioOnIssueDate) {
        this.indexRatioOnIssueDate = indexRatioOnIssueDate;
    }

    /**
     * "indirectBidderAccepted":"10000724000"
     * "indirectBidderAccepted":"6744260000"
     * "indirectBidderAccepted":"5137122200"
     */
    private Long indirectBidderAccepted;

    /**
     * Getter method for the indirectBidderAccepted property.
     */
    public Long getIndirectBidderAccepted () {
        return indirectBidderAccepted;
    }

    public static final String INDIRECTBIDDERACCEPTED = "indirectBidderAccepted";

    /**
     * Setter method for the indirectBidderAccepted property.
     */
    public void setIndirectBidderAccepted (@Changeable(INDIRECTBIDDERACCEPTED) Long indirectBidderAccepted) {
        this.indirectBidderAccepted = indirectBidderAccepted;
    }

    /**
     * "indirectBidderTendered":"15587325000"
     * "indirectBidderTendered":"10126880000"
     * "indirectBidderTendered":"6288113400"
     *
     * https://www.investopedia.com/terms/i/indirect-bidder.asp
     */
    private Long indirectBidderTendered;

    /**
     * Getter method for the indirectBidderTendered property.
     */
    public Long getIndirectBidderTendered () {
        return indirectBidderTendered;
    }

    public static final String INDIRECTBIDDERTENDERED = "indirectBidderTendered";

    /**
     * Setter method for the indirectBidderTendered property.
     */
    public void setIndirectBidderTendered (@Changeable(INDIRECTBIDDERTENDERED) Long indirectBidderTendered) {
        this.indirectBidderTendered = indirectBidderTendered;
    }

    /**
     * "interestPaymentFrequency":"None"
     * "interestPaymentFrequency":"Semi-Annual"
     *
     * @TODO Use and enum?
     */
    private String interestPaymentFrequency;

    /**
     * Getter method for the interestPaymentFrequency property.
     */
    public String getInterestPaymentFrequency () {
        return interestPaymentFrequency;
    }

    public static final String INTERESTPAYMENTFREQUENCY = "interestPaymentFrequency";

    /**
     * Setter method for the interestPaymentFrequency property.
     */
    public void setInterestPaymentFrequency (@Changeable(INTERESTPAYMENTFREQUENCY) String interestPaymentFrequency) {
        this.interestPaymentFrequency = interestPaymentFrequency;
    }

    /**
     * "lowDiscountRate":"0.050000"
     */
    private BigDecimal lowDiscountRate;

    /**
     * Getter method for the lowDiscountRate property.
     */
    public BigDecimal getLowDiscountRate () {
        return lowDiscountRate;
    }

    public static final String LOWDISCOUNTRATE = "lowDiscountRate";

    /**
     * Setter method for the lowDiscountRate property.
     */
    public void setLowDiscountRate (@Changeable(LOWDISCOUNTRATE) BigDecimal lowDiscountRate) {
        this.lowDiscountRate = lowDiscountRate;
    }

    private BigDecimal lowInvestmentRate;

    /**
     * Getter method for the lowInvestmentRate property.
     */
    public BigDecimal getLowInvestmentRate () {
        return lowInvestmentRate;
    }

    public static final String LOWINVESTMENTRATE = "lowInvestmentRate";

    /**
     * Setter method for the lowInvestmentRate property.
     */
    public void setLowInvestmentRate (@Changeable(LOWINVESTMENTRATE) BigDecimal lowInvestmentRate) {
        this.lowInvestmentRate = lowInvestmentRate;
    }

    /**
     * "lowPrice":""
     */
    private BigDecimal lowPrice;

    /**
     * Getter method for the lowPrice property.
     */
    public BigDecimal getLowPrice () {
        return lowPrice;
    }

    public static final String LOWPRICE = "lowPrice";

    /**
     * Setter method for the lowPrice property.
     */
    public void setLowPrice (@Changeable(LOWPRICE) BigDecimal lowPrice) {
        this.lowPrice = lowPrice;
    }

    /**
     * https://www.investopedia.com/terms/d/discountmargin.asp
     *
     *
     */
    private BigDecimal lowDiscountMargin;

    /**
     * Getter method for the lowDiscountMargin property.
     */
    public BigDecimal getLowDiscountMargin () {
        return lowDiscountMargin;
    }

    public static final String LOWDISCOUNTMARGIN = "lowDiscountMargin";

    /**
     * Setter method for the lowDiscountMargin property.
     */
    public void setLowDiscountMargin (@Changeable(LOWDISCOUNTMARGIN) BigDecimal lowDiscountMargin) {
        this.lowDiscountMargin = lowDiscountMargin;
    }

    /**
     * "lowYield":"0.302000"
     * "lowYield":"0.660000"
     * "lowYield":"0.900000"
     */
    private BigDecimal lowYield;

    /**
     * Getter method for the lowYield property.
     */
    public BigDecimal getLowYield () {
        return lowYield;
    }

    public static final String LOWYIELD = "lowYield";

    /**
     * Setter method for the lowYield property.
     */
    public void setLowYield (@Changeable(LOWYIELD) BigDecimal lowYield) {
        this.lowYield = lowYield;
    }

    /**
     * "maturingDate":"2014-02-11T00:00:00"
     */
    private Date maturingDate;

    /**
     * Getter method for the maturingDate property.
     */
    public Date getMaturingDate () {
        return maturingDate;
    }

    public static final String MATURINGDATE = "maturingDate";

    /**
     * Setter method for the maturingDate property.
     */
    public void setMaturingDate (@Changeable(MATURINGDATE) Date maturingDate) {
        this.maturingDate = maturingDate;
    }

    /**
     * "maximumCompetitiveAward":"17500000000"
     * "maximumCompetitiveAward":"4900000000"
     * "maximumCompetitiveAward":"3850000000"
     * "maximumCompetitiveAward":"2450000000"
     *
     * This looks like it should be a Long or Integer type however since it has to do with money the type has been set
     * to BigDecimal.
     */
    private BigDecimal maximumCompetitiveAward;

    /**
     * Getter method for the maximumCompetitiveAward property.
     */
    public BigDecimal getMaximumCompetitiveAward () {
        return maximumCompetitiveAward;
    }

    public static final String MAXIMUMCOMPETITIVEAWARD = "maximumCompetitiveAward";

    /**
     * Setter method for the maximumCompetitiveAward property.
     */
    public void setMaximumCompetitiveAward (@Changeable(MAXIMUMCOMPETITIVEAWARD) BigDecimal maximumCompetitiveAward) {
        this.maximumCompetitiveAward = maximumCompetitiveAward;
    }

    /**
     * "maximumNoncompetitiveAward":"5000000"
     *
     * This looks like it should be a Long or Integer type however since it has to do with money the type has been set
     * to BigDecimal.
     */
    private BigDecimal maximumNoncompetitiveAward;

    /**
     * Getter method for the maximumNoncompetitiveAward property.
     */
    public BigDecimal getMaximumNoncompetitiveAward () {
        return maximumNoncompetitiveAward;
    }

    public static final String MAXIMUMNONCOMPETITIVEAWARD = "maximumNoncompetitiveAward";

    /**
     * Setter method for the maximumNoncompetitiveAward property.
     */
    public void setMaximumNoncompetitiveAward (@Changeable(MAXIMUMNONCOMPETITIVEAWARD) BigDecimal maximumNoncompetitiveAward) {
        this.maximumNoncompetitiveAward = maximumNoncompetitiveAward;
    }

    /**
     * "maximumSingleBid":"17500000000"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * "maximumSingleBid":"4900000000"
     * "maximumSingleBid":"3850000000"
     * "maximumSingleBid":"2450000000"
     *
     * This looks like it should be a Long or Integer type however since it has to do with money the type has been set
     * to BigDecimal.
     */
    private BigDecimal maximumSingleBid;

    /**
     * Getter method for the maximumSingleBid property.
     */
    public BigDecimal getMaximumSingleBid () {
        return maximumSingleBid;
    }

    public static final String MAXIMUMSINGLEBID = "maximumSingleBid";

    /**
     * Setter method for the maximumSingleBid property.
     */
    public void setMaximumSingleBid (@Changeable(MAXIMUMSINGLEBID) BigDecimal maximumSingleBid) {
        this.maximumSingleBid = maximumSingleBid;
    }

    /**
     * "minimumBidAmount":"100"
     *
     * This looks like it should be a Long or Integer type however since it has to do with money the type has been set
     * to BigDecimal.
     */
    private BigDecimal minimumBidAmount;

    /**
     * Getter method for the minimumBidAmount property.
     */
    public BigDecimal getMinimumBidAmount () {
        return minimumBidAmount;
    }

    public static final String MINIMUMBIDAMOUNT = "minimumBidAmount";

    /**
     * Setter method for the minimumBidAmount property.
     */
    public void setMinimumBidAmount (@Changeable(MINIMUMBIDAMOUNT) BigDecimal minimumBidAmount) {
        this.minimumBidAmount = minimumBidAmount;
    }

    /**
     * "minimumStripAmount":"100"
     */
    private Integer minimumStripAmount;

    /**
     * Getter method for the minimumStripAmount property.
     */
    public Integer getMinimumStripAmount () {
        return minimumStripAmount;
    }

    public static final String MINIMUMSTRIPAMOUNT = "minimumStripAmount";

    /**
     * Setter method for the minimumStripAmount property.
     */
    public void setMinimumStripAmount (@Changeable(MINIMUMSTRIPAMOUNT) Integer minimumStripAmount) {
        this.minimumStripAmount = minimumStripAmount;
    }

    /**
     * "minimumToIssue":"100"
     *
     * @TODO Should this be an Integer?
     */
    private Integer minimumToIssue;

    /**
     * Getter method for the minimumToIssue property.
     */
    public Integer getMinimumToIssue () {
        return minimumToIssue;
    }

    public static final String MINIMUMTOISSUE = "minimumToIssue";

    /**
     * Setter method for the minimumToIssue property.
     */
    public void setMinimumToIssue (@Changeable(MINIMUMTOISSUE) Integer minimumToIssue) {
        this.minimumToIssue = minimumToIssue;
    }

    /**
     * "multiplesToBid":"100"
     */
    private Integer multiplesToBid;

    /**
     * Getter method for the multiplesToBid property.
     */
    public Integer getMultiplesToBid () {
        return multiplesToBid;
    }

    public static final String MULTIPLESTOBID = "multiplesToBid";

    /**
     * Setter method for the multiplesToBid property.
     */
    public void setMultiplesToBid (@Changeable(MULTIPLESTOBID) Integer multiplesToBid) {
        this.multiplesToBid = multiplesToBid;
    }

    /**
     * "multiplesToIssue":"100"
     *
     * @TODO Not sure if this should be an integer.
     */
    private Integer multiplesToIssue;

    /**
     * Getter method for the multiplesToIssue property.
     */
    public Integer getMultiplesToIssue () {
        return multiplesToIssue;
    }

    public static final String MULTIPLESTOISSUE = "multiplesToIssue";

    /**
     * Setter method for the multiplesToIssue property.
     */
    public void setMultiplesToIssue (@Changeable(MULTIPLESTOISSUE) Integer multiplesToIssue) {
        this.multiplesToIssue = multiplesToIssue;
    }

    /**
     * "nlpExclusionAmount":"29500000000"
     *
     * "nlpExclusionAmount":"10500000000"
     * "nlpExclusionAmount":"4600000000"
     * "nlpExclusionAmount":"0"
     */
    private Long nlpExclusionAmount;

    /**
     * Getter method for the nlpExclusionAmount property.
     */
    public Long getNlpExclusionAmount () {
        return nlpExclusionAmount;
    }

    public static final String NLPEXCLUSIONAMOUNT = "nlpExclusionAmount";

    /**
     * Setter method for the nlpExclusionAmount property.
     */
    public void setNlpExclusionAmount (@Changeable(NLPEXCLUSIONAMOUNT) Long nlpExclusionAmount) {
        this.nlpExclusionAmount = nlpExclusionAmount;
    }

    /**
     * "nlpReportingThreshold":"17500000000"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * "nlpReportingThreshold":"4900000000"
     * "nlpReportingThreshold":"3850000000"
     * "nlpReportingThreshold":"2450000000"
     */
    private String nlpReportingThreshold;

    /**
     * Getter method for the nlpReportingThreshold property.
     */
    public String getNlpReportingThreshold () {
        return nlpReportingThreshold;
    }

    public static final String NLPREPORTINGTHRESHOLD = "nlpReportingThreshold";

    /**
     * Setter method for the nlpReportingThreshold property.
     */
    public void setNlpReportingThreshold (@Changeable(NLPREPORTINGTHRESHOLD) String nlpReportingThreshold) {
        this.nlpReportingThreshold = nlpReportingThreshold;
    }

    /**
     * "noncompetitiveAccepted":"42919400"
     * "noncompetitiveAccepted":"16102700"
     * "noncompetitiveAccepted":"23681800"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * @TODO: Use an Integer type?
     */
    private Long noncompetitiveAccepted;

    /**
     * Getter method for the noncompetitiveAccepted property.
     */
    public Long getNoncompetitiveAccepted () {
        return noncompetitiveAccepted;
    }

    public static final String NONCOMPETITIVEACCEPTED = "noncompetitiveAccepted";

    /**
     * Setter method for the noncompetitiveAccepted property.
     */
    public void setNoncompetitiveAccepted (@Changeable(NONCOMPETITIVEACCEPTED) Long noncompetitiveAccepted) {
        this.noncompetitiveAccepted = noncompetitiveAccepted;
    }

    /**
     * "noncompetitiveTendersAccepted":"Yes"
     */
    private Boolean noncompetitiveTendersAccepted;

    /**
     * Getter method for the noncompetitiveTendersAccepted property.
     */
    public Boolean getNoncompetitiveTendersAccepted () {
        return noncompetitiveTendersAccepted;
    }

    public static final String NONCOMPETITIVETENDERSACCEPTED = "noncompetitiveTendersAccepted";

    /**
     * Setter method for the noncompetitiveTendersAccepted property.
     */
    public void setNoncompetitiveTendersAccepted (@Changeable(NONCOMPETITIVETENDERSACCEPTED) Boolean noncompetitiveTendersAccepted) {
        this.noncompetitiveTendersAccepted = noncompetitiveTendersAccepted;
    }

    /**
     * "offeringAmount":"50000000000"
     * 
     * "offeringAmount":"14000000000"
     * "offeringAmount":"11000000000"
     * "offeringAmount":"7000000000"
     *
     * @see <a href="https://definedterm.com/offering_amount">Offering Amount </a>
     */
    private Long offeringAmount;

    /**
     * Getter method for the offeringAmount property.
     */
    public Long getOfferingAmount () {
        return offeringAmount;
    }

    public static final String OFFERINGAMOUNT = "offeringAmount";

    /**
     * Setter method for the offeringAmount property.
     */
    public void setOfferingAmount (@Changeable(OFFERINGAMOUNT) Long offeringAmount) {
        this.offeringAmount = offeringAmount;
    }

    /**
     * "originalCusip":""
     */
    private String originalCusip;

    /**
     * Getter method for the originalCusip property.
     */
    public String getOriginalCusip () {
        return originalCusip;
    }

    public static final String ORIGINALCUSIP = "originalCusip";

    /**
     * Setter method for the originalCusip property.
     */
    public void setOriginalCusip (@Changeable(ORIGINALCUSIP) String originalCusip) {
        this.originalCusip = originalCusip;
    }

    /**
     * "originalDatedDate":"2017-04-15T00:00:00"
     * "originalDatedDate":"2018-01-15T00:00:00"
     * 
     */
    private Date originalDatedDate;

    /**
     * Getter method for the originalDatedDate property.
     */
    public Date getOriginalDatedDate () {
        return originalDatedDate;
    }

    public static final String ORIGINALDATEDDATE = "originalDatedDate";

    /**
     * Setter method for the originalDatedDate property.
     */
    public void setOriginalDatedDate (@Changeable(ORIGINALDATEDDATE) Date originalDatedDate) {
        this.originalDatedDate = originalDatedDate;
    }

    /**
     * "originalIssueDate":"2013-10-17T00:00:00"
     */
    private Date originalIssueDate;

    /**
     * Getter method for the originalIssueDate property.
     */
    public Date getOriginalIssueDate () {
        return originalIssueDate;
    }

    public static final String ORIGINALISSUEDATE = "originalIssueDate";

    /**
     * Setter method for the originalIssueDate property.
     */
    public void setOriginalIssueDate (@Changeable(ORIGINALISSUEDATE) Date originalIssueDate) {
        this.originalIssueDate = originalIssueDate;
    }

    /**
     * "originalSecurityTerm":"189-Day"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * "originalSecurityTerm":"5-Year"
     * "originalSecurityTerm":"10-Year"
     * "originalSecurityTerm":"30-Year"
     *
     * @TODO Convert to enum?
     */
    private String originalSecurityTerm;

    /**
     * Getter method for the originalSecurityTerm property.
     */
    public String getOriginalSecurityTerm () {
        return originalSecurityTerm;
    }

    public static final String ORIGINALSECURITYTERM = "originalSecurityTerm";

    /**
     * Setter method for the originalSecurityTerm property.
     */
    public void setOriginalSecurityTerm (@Changeable(ORIGINALSECURITYTERM) String originalSecurityTerm) {
        this.originalSecurityTerm = originalSecurityTerm;
    }

    /**
     * "pdfFilenameAnnouncement":"A_20140206_2.pdf"
     */
    private String pdfFilenameAnnouncement;

    /**
     * Getter method for the pdfFilenameAnnouncement property.
     */
    public String getPdfFilenameAnnouncement () {
        return pdfFilenameAnnouncement;
    }

    public static final String PDFFILENAMEANNOUNCEMENT = "pdfFilenameAnnouncement";

    /**
     * Setter method for the pdfFilenameAnnouncement property.
     */
    public void setPdfFilenameAnnouncement (@Changeable(PDFFILENAMEANNOUNCEMENT) String pdfFilenameAnnouncement) {
        this.pdfFilenameAnnouncement = pdfFilenameAnnouncement;
    }

    /**
     * "pdfFilenameCompetitiveResults":"R_20140210_3.pdf"
     */
    private String pdfFilenameCompetitiveResults;

    /**
     * Getter method for the pdfFilenameCompetitiveResults property.
     */
    public String getPdfFilenameCompetitiveResults () {
        return pdfFilenameCompetitiveResults;
    }

    public static final String PDFFILENAMECOMPETITIVERESULTS = "pdfFilenameCompetitiveResults";

    /**
     * Setter method for the pdfFilenameCompetitiveResults property.
     */
    public void setPdfFilenameCompetitiveResults (
        @Changeable(PDFFILENAMECOMPETITIVERESULTS) String pdfFilenameCompetitiveResults
    ) {
        this.pdfFilenameCompetitiveResults = pdfFilenameCompetitiveResults;
    }

    /**
     * "pdfFilenameNoncompetitiveResults":"NCR_20140210_3.pdf"
     */
    private String pdfFilenameNoncompetitiveResults;

    /**
     * Getter method for the pdfFilenameNoncompetitiveResults property.
     */
    public String getPdfFilenameNoncompetitiveResults () {
        return pdfFilenameNoncompetitiveResults;
    }

    public static final String PDFFILENAMENONCOMPETITIVERESULTS = "pdfFilenameNoncompetitiveResults";

    /**
     * Setter method for the pdfFilenameNoncompetitiveResults property.
     */
    public void setPdfFilenameNoncompetitiveResults (@Changeable(PDFFILENAMENONCOMPETITIVERESULTS) String pdfFilenameNoncompetitiveResults) {
        this.pdfFilenameNoncompetitiveResults = pdfFilenameNoncompetitiveResults;
    }

    /**
     * "pdfFilenameSpecialAnnouncement":""
     */
    private String pdfFilenameSpecialAnnouncement;

    /**
     * Getter method for the pdfFilenameSpecialAnnouncement property.
     */
    public String getPdfFilenameSpecialAnnouncement () {
        return pdfFilenameSpecialAnnouncement;
    }

    public static final String PDFFILENAMESPECIALANNOUNCEMENT = "pdfFilenameSpecialAnnouncement";

    /**
     * Setter method for the pdfFilenameSpecialAnnouncement property.
     */
    public void setPdfFilenameSpecialAnnouncement (@Changeable(PDFFILENAMESPECIALANNOUNCEMENT) String pdfFilenameSpecialAnnouncement) {
        this.pdfFilenameSpecialAnnouncement = pdfFilenameSpecialAnnouncement;
    }

    /**
     * "pricePer100":"99.982000"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * "pricePer100":"100.374263"
     * "pricePer100":"97.935177"
     * "pricePer100":"99.895379"
     */
    private BigDecimal pricePer100;

    /**
     * Getter method for the pricePer100 property.
     */
    public BigDecimal getPricePer100 () {
        return pricePer100;
    }

    public static final String PRICEPER100 = "pricePer100";

    /**
     * Setter method for the pricePer100 property.
     */
    public void setPricePer100 (@Changeable(PRICEPER100) BigDecimal pricePer100) {
        this.pricePer100 = pricePer100;
    }

    /**
     * "primaryDealerAccepted":"39736510000"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * "primaryDealerAccepted":"2249360000"
     * "primaryDealerAccepted":"2406640000"
     * "primaryDealerAccepted":"1571208000"
     */
    private Long primaryDealerAccepted;

    /**
     * Getter method for the primaryDealerAccepted property.
     */
    public Long getPrimaryDealerAccepted () {
        return primaryDealerAccepted;
    }

    public static final String PRIMARYDEALERACCEPTED = "primaryDealerAccepted";

    /**
     * Setter method for the primaryDealerAccepted property.
     */
    public void setPrimaryDealerAccepted (@Changeable(PRIMARYDEALERACCEPTED) Long primaryDealerAccepted) {
        this.primaryDealerAccepted = primaryDealerAccepted;
    }

    /**
     * "primaryDealerTendered":"145850000000"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * "primaryDealerTendered":"21067000000"
     * "primaryDealerTendered":"15818000000"
     * "primaryDealerTendered":"9152000000"
     */
    private Long primaryDealerTendered;

    /**
     * Getter method for the primaryDealerTendered property.
     */
    public Long getPrimaryDealerTendered () {
        return primaryDealerTendered;
    }

    public static final String PRIMARYDEALERTENDERED = "primaryDealerTendered";

    /**
     * Setter method for the primaryDealerTendered property.
     */
    public void setPrimaryDealerTendered (@Changeable(PRIMARYDEALERTENDERED) Long primaryDealerTendered) {
        this.primaryDealerTendered = primaryDealerTendered;
    }

    /**
     * "reopening":"Yes"
     */
    private Boolean reopening;

    /**
     * Getter method for the reopening property.
     */
    public Boolean getReopening () {
        return reopening;
    }

    public static final String REOPENING = "reopening";

    /**
     * Setter method for the reopening property.
     */
    public void setReopening (@Changeable(REOPENING) Boolean reopening) {
        this.reopening = reopening;
    }

    /**
     * "securityTermDayMonth":"72-Day"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * "securityTermDayMonth":"4-Month"
     * "securityTermDayMonth":"10-Month"
     * "securityTermDayMonth":"0-Month"
     */
    private String securityTermDayMonth;

    /**
     * Getter method for the securityTermDayMonth property.
     */
    public String getSecurityTermDayMonth () {
        return securityTermDayMonth;
    }

    public static final String SECURITYTERMDAYMONTH = "securityTermDayMonth";

    /**
     * Setter method for the securityTermDayMonth property.
     */
    public void setSecurityTermDayMonth (@Changeable(SECURITYTERMDAYMONTH) String securityTermDayMonth) {
        this.securityTermDayMonth = securityTermDayMonth;
    }

    /**
     * "securityTermWeekYear":"0-Week"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * "securityTermWeekYear":"4-Year"
     * "securityTermWeekYear":"9-Year"
     * "securityTermWeekYear":"30-Year"
     *
     * @TODO Can this be made an enum?
     */
    private String securityTermWeekYear;

    /**
     * Getter method for the securityTermWeekYear property.
     */
    public String getSecurityTermWeekYear () {
        return securityTermWeekYear;
    }

    public static final String SECURITYTERMWEEKYEAR = "securityTermWeekYear";

    /**
     * Setter method for the securityTermWeekYear property.
     */
    public void setSecurityTermWeekYear (@Changeable(SECURITYTERMWEEKYEAR) String securityTermWeekYear) {
        this.securityTermWeekYear = securityTermWeekYear;
    }

    /**
     * "series":""
     *
     * The following examples are from: https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     * 
     * "series":"X-2022"
     * "series":"A-2028"
     * "series":"TIPS of February 2048"
     */
    private String series;

    /**
     * Getter method for the series property.
     */
    public String getSeries () {
        return series;
    }

    public static final String SERIES = "series";

    /**
     * Setter method for the series property.
     */
    public void setSeries (@Changeable(SERIES) String series) {
        this.series = series;
    }

    /**
     * Examples from: https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * 0
     * 1706564400
     */
    private Long somaAccepted;

    /**
     * Getter method for the somaAccepted property.
     */
    public Long getSomaAccepted () {
        return somaAccepted;
    }

    public static final String SOMAACCEPTED = "somaAccepted";

    /**
     * Setter method for the somaAccepted property.
     */
    public void setSomaAccepted (@Changeable(SOMAACCEPTED) Long somaAccepted) {
        this.somaAccepted = somaAccepted;
    }

    /**
     * Example from: https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * "somaHoldings":"32047000000"
     * 
     */
    private Long somaHoldings;

    /**
     * Getter method for the somaHoldings property.
     */
    public Long getSomaHoldings () {
        return somaHoldings;
    }

    public static final String SOMAHOLDINGS = "somaHoldings";

    /**
     * Setter method for the somaHoldings property.
     */
    public void setSomaHoldings (@Changeable(SOMAHOLDINGS) Long somaHoldings) {
        this.somaHoldings = somaHoldings;
    }

    /**
     * "somaIncluded":"No"
     */
    private Boolean somaIncluded;

    /**
     * Getter method for the somaIncluded property.
     */
    public Boolean getSomaIncluded () {
        return somaIncluded;
    }

    public static final String SOMAINCLUDED = "somaIncluded";

    /**
     * Setter method for the somaIncluded property.
     */
    public void setSomaIncluded (@Changeable(SOMAINCLUDED) Boolean somaIncluded) {
        this.somaIncluded = somaIncluded;
    }

    /**
     * "somaTendered":"0"
     *
     * Examples from (same number in both cases):
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     *
     * and
     *
     * From: https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&format=json
     *
     * 0
     * 1706564400
     */
    private Long somaTendered;

    /**
     * Getter method for the somaTendered property.
     */
    public Long getSomaTendered () {
        return somaTendered;
    }

    public static final String SOMATENDERED = "somaTendered";

    /**
     * Setter method for the somaTendered property.
     */
    public void setSomaTendered (@Changeable(SOMATENDERED) Long somaTendered) {
        this.somaTendered = somaTendered;
    }

    /**
     * "spread":""
     *
     * @TODO Examples needed.
     */
    private BigDecimal spread;

    /**
     * Getter method for the spread property.
     */
    public BigDecimal getSpread () {
        return spread;
    }

    public static final String SPREAD = "spread";

    /**
     * Setter method for the spread property.
     */
    public void setSpread (@Changeable(SPREAD) BigDecimal spread) {
        this.spread = spread;
    }

    /**
     * "standardInterestPaymentPer1000":""
     * 
     * Examples from https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     * are blank so this needs to be revisited.
     *
     * @TODO Find some examples for this property.
     */
    private BigDecimal standardInterestPaymentPer1000;

    /**
     * Getter method for the standardInterestPaymentPer1000 property.
     */
    public BigDecimal getStandardInterestPaymentPer1000 () {
        return standardInterestPaymentPer1000;
    }

    public static final String STANDARDINTERESTPAYMENTPER1000 = "standardInterestPaymentPer1000";

    /**
     * Setter method for the standardInterestPaymentPer1000 property.
     */
    public void setStandardInterestPaymentPer1000 (@Changeable(STANDARDINTERESTPAYMENTPER1000) BigDecimal standardInterestPaymentPer1000) {
        this.standardInterestPaymentPer1000 = standardInterestPaymentPer1000;
    }

    /**
     * "strippable":""
     *
     * Examples from:
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     *
     * Yes
     */
    private Boolean strippable;

    /**
     * Getter method for the strippable property.
     */
    public Boolean getStrippable () {
        return strippable;
    }

    public static final String STRIPPABLE = "strippable";

    /**
     * Setter method for the strippable property.
     */
    public void setStrippable (@Changeable(STRIPPABLE) Boolean strippable) {
        this.strippable = strippable;
    }

    /**
     * "term":""
     *
     * Examples from:
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     *
     * 5-Year
     * 10-Year
     * 30-Year
     *
     * @TODO Consider making this an enum.
     */
    private String term;

    /**
     * Getter method for the term property.
     */
    public String getTerm () {
        return term;
    }

    public static final String TERM = "term";

    /**
     * Setter method for the term property.
     */
    public void setTerm (@Changeable(TERM) String term) {
        this.term = term;
    }

    /**
     * "tiinConversionFactorPer1000":""
     *
     * Examples from:
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     * 
     * 0.2569948550
     * 1.0135174440
     * 2.0276037980
     */
    private BigDecimal tiinConversionFactorPer1000;

    /**
     * Getter method for the tiinConversionFactorPer1000 property.
     */
    public BigDecimal getTiinConversionFactorPer1000 () {
        return tiinConversionFactorPer1000;
    }

    public static final String TIINCONVERSIONFACTORPER1000 = "tiinConversionFactorPer1000";

    /**
     * Setter method for the tiinConversionFactorPer1000 property.
     */
    public void setTiinConversionFactorPer1000 (@Changeable(TIINCONVERSIONFACTORPER1000) BigDecimal tiinConversionFactorPer1000) {
        this.tiinConversionFactorPer1000 = tiinConversionFactorPer1000;
    }

    /**
     * "tips":"No"
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     * 
     * Yes
     */
    private Boolean tips;

    /**
     * Getter method for the tips property.
     */
    public Boolean getTips () {
        return tips;
    }

    public static final String TIPS = "tips";

    /**
     * Setter method for the tips property.
     */
    public void setTips (@Changeable(TIPS) Boolean tips) {
        this.tips = tips;
    }

    /**
     * "totalAccepted":"50000285000"
     *
     * From: https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     *
     * totalAccepted
     * 
     * 14000003400
     * 11000002700
     * 8706576400
     */
    private Long totalAccepted;

    /**
     * Getter method for the totalAccepted property.
     */
    public Long getTotalAccepted () {
        return totalAccepted;
    }

    public static final String TOTALACCEPTED = "totalAccepted";

    /**
     * Setter method for the totalAccepted property.
     */
    public void setTotalAccepted (@Changeable(TOTALACCEPTED) Long totalAccepted) {
        this.totalAccepted = totalAccepted;
    }

    /**
     * "totalTendered":"169059960000"
     */
    private Long totalTendered;

    /**
     * Getter method for the totalTendered property.
     */
    public Long getTotalTendered () {
        return totalTendered;
    }

    public static final String TOTALTENDERED = "totalTendered";

    /**
     * Setter method for the totalTendered property.
     */
    public void setTotalTendered (@Changeable(TOTALTENDERED) Long totalTendered) {
        this.totalTendered = totalTendered;
    }

    /**
     * "treasuryDirectAccepted":"0"
     * 
     * 4078000
     *
     * https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=FRN&days=720&callback=?
     */
    private Long treasuryDirectAccepted;

    /**
     * Getter method for the treasuryDirectAccepted property.
     */
    public Long getTreasuryDirectAccepted () {
        return treasuryDirectAccepted;
    }

    public static final String TREASURYDIRECTACCEPTED = "treasuryDirectAccepted";

    /**
     * Setter method for the treasuryDirectAccepted property.
     */
    public void setTreasuryDirectAccepted (@Changeable(TREASURYDIRECTACCEPTED) Long treasuryDirectAccepted) {
        this.treasuryDirectAccepted = treasuryDirectAccepted;
    }

    /**
     * "treasuryDirectTendersAccepted":"No"
     */
    private Boolean treasuryDirectTendersAccepted;

    /**
     * Getter method for the treasuryDirectTendersAccepted property.
     */
    public Boolean getTreasuryDirectTendersAccepted () {
        return treasuryDirectTendersAccepted;
    }

    public static final String TREASURYDIRECTTENDERSACCEPTED = "treasuryDirectTendersAccepted";

    /**
     * Setter method for the treasuryDirectTendersAccepted property.
     */
    public void setTreasuryDirectTendersAccepted (@Changeable(TREASURYDIRECTTENDERSACCEPTED) Boolean treasuryDirectTendersAccepted) {
        this.treasuryDirectTendersAccepted = treasuryDirectTendersAccepted;
    }

    /**
     * "type":"CMB"
     *
     * @TODO: Use an enum.
     */
    private String type;

    /**
     * Getter method for the type property.
     */
    public String getType () {
        return type;
    }

    public static final String TYPE = "type";

    /**
     * Setter method for the type property.
     */
    public void setType (@Changeable(TYPE) String type) {
        this.type = type;
    }

    /**
     * "unadjustedAccruedInterestPer1000":""
     *
     * UnadjustedAccruedInterestPer1000 (from: https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?)
     * 0.257550
     * 1.008290
     * 0.359120
     */
    private BigDecimal unadjustedAccruedInterestPer1000;

    /**
     * Getter method for the unadjustedAccruedInterestPer1000 property.
     */
    public BigDecimal getUnadjustedAccruedInterestPer1000 () {
        return unadjustedAccruedInterestPer1000;
    }

    public static final String UNADJUSTEDACCRUEDINTERESTPER1000 = "unadjustedAccruedInterestPer1000";

    /**
     * Setter method for the unadjustedAccruedInterestPer1000 property.
     */
    public void setUnadjustedAccruedInterestPer1000 (@Changeable(UNADJUSTEDACCRUEDINTERESTPER1000) BigDecimal unadjustedAccruedInterestPer1000) {
        this.unadjustedAccruedInterestPer1000 = unadjustedAccruedInterestPer1000;
    }

    /**
     * "unadjustedPrice":""
     *
     * From: https://www.treasurydirect.gov/TA_WS/securities/auctioned?limitByTerm=true&type=TIPS&days=720&callback=?
     *
     * unadjustedPrice
     * 98.957196
     * 97.511975
     * 99.922358
     */
    private BigDecimal unadjustedPrice;

    /**
     * Getter method for the unadjustedPrice property.
     */
    public BigDecimal getUnadjustedPrice () {
        return unadjustedPrice;
    }

    public static final String UNADJUSTEDPRICE = "unadjustedPrice";

    /**
     * Setter method for the unadjustedPrice property.
     */
    public void setUnadjustedPrice (@Changeable(UNADJUSTEDPRICE) BigDecimal unadjustedPrice) {
        this.unadjustedPrice = unadjustedPrice;
    }

    /**
     * "updatedTimestamp":"2014-03-07T12:05:06"
     */
    private Date updatedTimestamp;

    /**
     * Getter method for the updatedTimestamp property.
     */
    public Date getUpdatedTimestamp () {
        return updatedTimestamp;
    }

    public static final String UPDATEDTIMESTAMP = "updatedTimestamp";

    /**
     * Setter method for the updatedTimestamp property.
     */
    public void setUpdatedTimestamp (@Changeable(UPDATEDTIMESTAMP) Date updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }

    /**
     * "xmlFilenameAnnouncement":"A_20140206_2.xml"
     */
    private String xmlFilenameAnnouncement;

    /**
     * Getter method for the xmlFilenameAnnouncement property.
     */
    public String getXmlFilenameAnnouncement () {
        return xmlFilenameAnnouncement;
    }

    public static final String XMLFILENAMEANNOUNCEMENT = "xmlFilenameAnnouncement";

    /**
     * Setter method for the xmlFilenameAnnouncement property.
     */
    public void setXmlFilenameAnnouncement (@Changeable(XMLFILENAMEANNOUNCEMENT) String xmlFilenameAnnouncement) {
        this.xmlFilenameAnnouncement = xmlFilenameAnnouncement;
    }

    /**
     * "xmlFilenameCompetitiveResults":"R_20140210_3.xml"
     */
    private String xmlFilenameCompetitiveResults;

    /**
     * Getter method for the xmlFilenameCompetitiveResults property.
     */
    public String getXmlFilenameCompetitiveResults () {
        return xmlFilenameCompetitiveResults;
    }

    public static final String XMLFILENAMECOMPETITIVERESULTS = "xmlFilenameCompetitiveResults";

    /**
     * Setter method for the xmlFilenameCompetitiveResults property.
     */
    public void setXmlFilenameCompetitiveResults (@Changeable(XMLFILENAMECOMPETITIVERESULTS) String xmlFilenameCompetitiveResults) {
        this.xmlFilenameCompetitiveResults = xmlFilenameCompetitiveResults;
    }

    /**
     * xmlFilenameSpecialAnnouncement:""
     */
    private String xmlFilenameSpecialAnnouncement;

    /**
     * Getter method for the xmlFilenameSpecialAnnouncement property.
     */
    public String getXmlFilenameSpecialAnnouncement () {
        return xmlFilenameSpecialAnnouncement;
    }

    public static final String XMLFILENAMESPECIALANNOUNCEMENT = "xmlFilenameSpecialAnnouncement";

    /**
     * Setter method for the xmlFilenameSpecialAnnouncement property.
     */
    public void setXmlFilenameSpecialAnnouncement (@Changeable(XMLFILENAMESPECIALANNOUNCEMENT) String xmlFilenameSpecialAnnouncement) {
        this.xmlFilenameSpecialAnnouncement = xmlFilenameSpecialAnnouncement;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((accruedInterestPer100 == null) ? 0 : accruedInterestPer100.hashCode());
        result = prime * result + ((accruedInterestPer1000 == null) ? 0 : accruedInterestPer1000.hashCode());
        result = prime * result
                + ((adjustedAccruedInterestPer1000 == null) ? 0 : adjustedAccruedInterestPer1000.hashCode());
        result = prime * result + ((adjustedPrice == null) ? 0 : adjustedPrice.hashCode());
        result = prime * result + ((allocationPercentage == null) ? 0 : allocationPercentage.hashCode());
        result = prime * result
                + ((allocationPercentageDecimals == null) ? 0 : allocationPercentageDecimals.hashCode());
        result = prime * result + ((announcedCusip == null) ? 0 : announcedCusip.hashCode());
        result = prime * result + ((announcementDate == null) ? 0 : announcementDate.hashCode());
        result = prime * result + ((auctionDate == null) ? 0 : auctionDate.hashCode());
        result = prime * result + ((auctionDateYear == null) ? 0 : auctionDateYear.hashCode());
        result = prime * result + ((auctionFormat == null) ? 0 : auctionFormat.hashCode());
        result = prime * result + ((averageMedianDiscountMargin == null) ? 0 : averageMedianDiscountMargin.hashCode());
        result = prime * result + ((averageMedianDiscountRate == null) ? 0 : averageMedianDiscountRate.hashCode());
        result = prime * result + ((averageMedianInvestmentRate == null) ? 0 : averageMedianInvestmentRate.hashCode());
        result = prime * result + ((averageMedianPrice == null) ? 0 : averageMedianPrice.hashCode());
        result = prime * result + ((averageMedianYield == null) ? 0 : averageMedianYield.hashCode());
        result = prime * result + ((backDated == null) ? 0 : backDated.hashCode());
        result = prime * result + ((backDatedDate == null) ? 0 : backDatedDate.hashCode());
        result = prime * result + ((bidToCoverRatio == null) ? 0 : bidToCoverRatio.hashCode());
        result = prime * result + ((callDate == null) ? 0 : callDate.hashCode());
        result = prime * result + ((callable == null) ? 0 : callable.hashCode());
        result = prime * result + ((calledDate == null) ? 0 : calledDate.hashCode());
        result = prime * result + ((cashManagementBillCMB == null) ? 0 : cashManagementBillCMB.hashCode());
        result = prime * result + ((closingTimeCompetitive == null) ? 0 : closingTimeCompetitive.hashCode());
        result = prime * result + ((closingTimeNoncompetitive == null) ? 0 : closingTimeNoncompetitive.hashCode());
        result = prime * result + ((competitiveAccepted == null) ? 0 : competitiveAccepted.hashCode());
        result = prime * result + ((competitiveBidDecimals == null) ? 0 : competitiveBidDecimals.hashCode());
        result = prime * result + ((competitiveTendered == null) ? 0 : competitiveTendered.hashCode());
        result = prime * result + ((competitiveTendersAccepted == null) ? 0 : competitiveTendersAccepted.hashCode());
        result = prime * result + ((corpusCusip == null) ? 0 : corpusCusip.hashCode());
        result = prime * result + ((cpiBaseReferencePeriod == null) ? 0 : cpiBaseReferencePeriod.hashCode());
        result = prime * result + ((currentlyOutstanding == null) ? 0 : currentlyOutstanding.hashCode());
        result = prime * result + ((cusip == null) ? 0 : cusip.hashCode());
        result = prime * result + ((datedDate == null) ? 0 : datedDate.hashCode());
        result = prime * result + ((directBidderAccepted == null) ? 0 : directBidderAccepted.hashCode());
        result = prime * result + ((directBidderTendered == null) ? 0 : directBidderTendered.hashCode());
        result = prime * result + ((estimatedAmountOfPubliclyHeldMaturingSecuritiesByType == null) ? 0
                : estimatedAmountOfPubliclyHeldMaturingSecuritiesByType.hashCode());
        result = prime * result + ((fimaIncluded == null) ? 0 : fimaIncluded.hashCode());
        result = prime * result + ((fimaNoncompetitiveAccepted == null) ? 0 : fimaNoncompetitiveAccepted.hashCode());
        result = prime * result + ((fimaNoncompetitiveTendered == null) ? 0 : fimaNoncompetitiveTendered.hashCode());
        result = prime * result + ((firstInterestPaymentDate == null) ? 0 : firstInterestPaymentDate.hashCode());
        result = prime * result + ((firstInterestPeriod == null) ? 0 : firstInterestPeriod.hashCode());
        result = prime * result + ((floatingRate == null) ? 0 : floatingRate.hashCode());
        result = prime * result + ((frnIndexDeterminationDate == null) ? 0 : frnIndexDeterminationDate.hashCode());
        result = prime * result + ((frnIndexDeterminationRate == null) ? 0 : frnIndexDeterminationRate.hashCode());
        result = prime * result + ((highDiscountMargin == null) ? 0 : highDiscountMargin.hashCode());
        result = prime * result + ((highDiscountRate == null) ? 0 : highDiscountRate.hashCode());
        result = prime * result + ((highInvestmentRate == null) ? 0 : highInvestmentRate.hashCode());
        result = prime * result + ((highPrice == null) ? 0 : highPrice.hashCode());
        result = prime * result + ((highYield == null) ? 0 : highYield.hashCode());
        result = prime * result + ((indexRatioOnIssueDate == null) ? 0 : indexRatioOnIssueDate.hashCode());
        result = prime * result + ((indirectBidderAccepted == null) ? 0 : indirectBidderAccepted.hashCode());
        result = prime * result + ((indirectBidderTendered == null) ? 0 : indirectBidderTendered.hashCode());
        result = prime * result + ((interestPaymentFrequency == null) ? 0 : interestPaymentFrequency.hashCode());
        result = prime * result + ((interestRate == null) ? 0 : interestRate.hashCode());
        result = prime * result + ((issueDate == null) ? 0 : issueDate.hashCode());
        result = prime * result + ((lowDiscountMargin == null) ? 0 : lowDiscountMargin.hashCode());
        result = prime * result + ((lowDiscountRate == null) ? 0 : lowDiscountRate.hashCode());
        result = prime * result + ((lowInvestmentRate == null) ? 0 : lowInvestmentRate.hashCode());
        result = prime * result + ((lowPrice == null) ? 0 : lowPrice.hashCode());
        result = prime * result + ((lowYield == null) ? 0 : lowYield.hashCode());
        result = prime * result + ((maturingDate == null) ? 0 : maturingDate.hashCode());
        result = prime * result + ((maturityDate == null) ? 0 : maturityDate.hashCode());
        result = prime * result + ((maximumCompetitiveAward == null) ? 0 : maximumCompetitiveAward.hashCode());
        result = prime * result + ((maximumNoncompetitiveAward == null) ? 0 : maximumNoncompetitiveAward.hashCode());
        result = prime * result + ((maximumSingleBid == null) ? 0 : maximumSingleBid.hashCode());
        result = prime * result + ((minimumBidAmount == null) ? 0 : minimumBidAmount.hashCode());
        result = prime * result + ((minimumStripAmount == null) ? 0 : minimumStripAmount.hashCode());
        result = prime * result + ((minimumToIssue == null) ? 0 : minimumToIssue.hashCode());
        result = prime * result + ((multiplesToBid == null) ? 0 : multiplesToBid.hashCode());
        result = prime * result + ((multiplesToIssue == null) ? 0 : multiplesToIssue.hashCode());
        result = prime * result + ((nlpExclusionAmount == null) ? 0 : nlpExclusionAmount.hashCode());
        result = prime * result + ((nlpReportingThreshold == null) ? 0 : nlpReportingThreshold.hashCode());
        result = prime * result + ((noncompetitiveAccepted == null) ? 0 : noncompetitiveAccepted.hashCode());
        result = prime * result
                + ((noncompetitiveTendersAccepted == null) ? 0 : noncompetitiveTendersAccepted.hashCode());
        result = prime * result + ((offeringAmount == null) ? 0 : offeringAmount.hashCode());
        result = prime * result + ((originalCusip == null) ? 0 : originalCusip.hashCode());
        result = prime * result + ((originalDatedDate == null) ? 0 : originalDatedDate.hashCode());
        result = prime * result + ((originalIssueDate == null) ? 0 : originalIssueDate.hashCode());
        result = prime * result + ((originalSecurityTerm == null) ? 0 : originalSecurityTerm.hashCode());
        result = prime * result + ((pdfFilenameAnnouncement == null) ? 0 : pdfFilenameAnnouncement.hashCode());
        result = prime * result
                + ((pdfFilenameCompetitiveResults == null) ? 0 : pdfFilenameCompetitiveResults.hashCode());
        result = prime * result
                + ((pdfFilenameNoncompetitiveResults == null) ? 0 : pdfFilenameNoncompetitiveResults.hashCode());
        result = prime * result
                + ((pdfFilenameSpecialAnnouncement == null) ? 0 : pdfFilenameSpecialAnnouncement.hashCode());
        result = prime * result + ((pricePer100 == null) ? 0 : pricePer100.hashCode());
        result = prime * result + ((primaryDealerAccepted == null) ? 0 : primaryDealerAccepted.hashCode());
        result = prime * result + ((primaryDealerTendered == null) ? 0 : primaryDealerTendered.hashCode());
        result = prime * result + ((refCpiOnDatedDate == null) ? 0 : refCpiOnDatedDate.hashCode());
        result = prime * result + ((refCpiOnIssueDate == null) ? 0 : refCpiOnIssueDate.hashCode());
        result = prime * result + ((reopening == null) ? 0 : reopening.hashCode());
        result = prime * result + ((securityTerm == null) ? 0 : securityTerm.hashCode());
        result = prime * result + ((securityTermDayMonth == null) ? 0 : securityTermDayMonth.hashCode());
        result = prime * result + ((securityTermWeekYear == null) ? 0 : securityTermWeekYear.hashCode());
        result = prime * result + ((securityType == null) ? 0 : securityType.hashCode());
        result = prime * result + ((series == null) ? 0 : series.hashCode());
        result = prime * result + ((somaAccepted == null) ? 0 : somaAccepted.hashCode());
        result = prime * result + ((somaHoldings == null) ? 0 : somaHoldings.hashCode());
        result = prime * result + ((somaIncluded == null) ? 0 : somaIncluded.hashCode());
        result = prime * result + ((somaTendered == null) ? 0 : somaTendered.hashCode());
        result = prime * result + ((spread == null) ? 0 : spread.hashCode());
        result = prime * result
                + ((standardInterestPaymentPer1000 == null) ? 0 : standardInterestPaymentPer1000.hashCode());
        result = prime * result + ((strippable == null) ? 0 : strippable.hashCode());
        result = prime * result + ((term == null) ? 0 : term.hashCode());
        result = prime * result + ((tiinConversionFactorPer1000 == null) ? 0 : tiinConversionFactorPer1000.hashCode());
        result = prime * result + ((tips == null) ? 0 : tips.hashCode());
        result = prime * result + ((totalAccepted == null) ? 0 : totalAccepted.hashCode());
        result = prime * result + ((totalTendered == null) ? 0 : totalTendered.hashCode());
        result = prime * result + ((treasuryDirectAccepted == null) ? 0 : treasuryDirectAccepted.hashCode());
        result = prime * result
                + ((treasuryDirectTendersAccepted == null) ? 0 : treasuryDirectTendersAccepted.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result
                + ((unadjustedAccruedInterestPer1000 == null) ? 0 : unadjustedAccruedInterestPer1000.hashCode());
        result = prime * result + ((unadjustedPrice == null) ? 0 : unadjustedPrice.hashCode());
        result = prime * result + ((updatedTimestamp == null) ? 0 : updatedTimestamp.hashCode());
        result = prime * result + ((xmlFilenameAnnouncement == null) ? 0 : xmlFilenameAnnouncement.hashCode());
        result = prime * result
                + ((xmlFilenameCompetitiveResults == null) ? 0 : xmlFilenameCompetitiveResults.hashCode());
        result = prime * result
                + ((xmlFilenameSpecialAnnouncement == null) ? 0 : xmlFilenameSpecialAnnouncement.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Security other = (Security) obj;
        if (accruedInterestPer100 == null) {
            if (other.accruedInterestPer100 != null)
                return false;
        } else if (!accruedInterestPer100.equals(other.accruedInterestPer100))
            return false;
        if (accruedInterestPer1000 == null) {
            if (other.accruedInterestPer1000 != null)
                return false;
        } else if (!accruedInterestPer1000.equals(other.accruedInterestPer1000))
            return false;
        if (adjustedAccruedInterestPer1000 == null) {
            if (other.adjustedAccruedInterestPer1000 != null)
                return false;
        } else if (!adjustedAccruedInterestPer1000.equals(other.adjustedAccruedInterestPer1000))
            return false;
        if (adjustedPrice == null) {
            if (other.adjustedPrice != null)
                return false;
        } else if (!adjustedPrice.equals(other.adjustedPrice))
            return false;
        if (allocationPercentage == null) {
            if (other.allocationPercentage != null)
                return false;
        } else if (!allocationPercentage.equals(other.allocationPercentage))
            return false;
        if (allocationPercentageDecimals == null) {
            if (other.allocationPercentageDecimals != null)
                return false;
        } else if (!allocationPercentageDecimals.equals(other.allocationPercentageDecimals))
            return false;
        if (announcedCusip == null) {
            if (other.announcedCusip != null)
                return false;
        } else if (!announcedCusip.equals(other.announcedCusip))
            return false;
        if (announcementDate == null) {
            if (other.announcementDate != null)
                return false;
        } else if (!announcementDate.equals(other.announcementDate))
            return false;
        if (auctionDate == null) {
            if (other.auctionDate != null)
                return false;
        } else if (!auctionDate.equals(other.auctionDate))
            return false;
        if (auctionDateYear == null) {
            if (other.auctionDateYear != null)
                return false;
        } else if (!auctionDateYear.equals(other.auctionDateYear))
            return false;
        if (auctionFormat == null) {
            if (other.auctionFormat != null)
                return false;
        } else if (!auctionFormat.equals(other.auctionFormat))
            return false;
        if (averageMedianDiscountMargin == null) {
            if (other.averageMedianDiscountMargin != null)
                return false;
        } else if (!averageMedianDiscountMargin.equals(other.averageMedianDiscountMargin))
            return false;
        if (averageMedianDiscountRate == null) {
            if (other.averageMedianDiscountRate != null)
                return false;
        } else if (!averageMedianDiscountRate.equals(other.averageMedianDiscountRate))
            return false;
        if (averageMedianInvestmentRate == null) {
            if (other.averageMedianInvestmentRate != null)
                return false;
        } else if (!averageMedianInvestmentRate.equals(other.averageMedianInvestmentRate))
            return false;
        if (averageMedianPrice == null) {
            if (other.averageMedianPrice != null)
                return false;
        } else if (!averageMedianPrice.equals(other.averageMedianPrice))
            return false;
        if (averageMedianYield == null) {
            if (other.averageMedianYield != null)
                return false;
        } else if (!averageMedianYield.equals(other.averageMedianYield))
            return false;
        if (backDated == null) {
            if (other.backDated != null)
                return false;
        } else if (!backDated.equals(other.backDated))
            return false;
        if (backDatedDate == null) {
            if (other.backDatedDate != null)
                return false;
        } else if (!backDatedDate.equals(other.backDatedDate))
            return false;
        if (bidToCoverRatio == null) {
            if (other.bidToCoverRatio != null)
                return false;
        } else if (!bidToCoverRatio.equals(other.bidToCoverRatio))
            return false;
        if (callDate == null) {
            if (other.callDate != null)
                return false;
        } else if (!callDate.equals(other.callDate))
            return false;
        if (callable == null) {
            if (other.callable != null)
                return false;
        } else if (!callable.equals(other.callable))
            return false;
        if (calledDate == null) {
            if (other.calledDate != null)
                return false;
        } else if (!calledDate.equals(other.calledDate))
            return false;
        if (cashManagementBillCMB == null) {
            if (other.cashManagementBillCMB != null)
                return false;
        } else if (!cashManagementBillCMB.equals(other.cashManagementBillCMB))
            return false;
        if (closingTimeCompetitive == null) {
            if (other.closingTimeCompetitive != null)
                return false;
        } else if (!closingTimeCompetitive.equals(other.closingTimeCompetitive))
            return false;
        if (closingTimeNoncompetitive == null) {
            if (other.closingTimeNoncompetitive != null)
                return false;
        } else if (!closingTimeNoncompetitive.equals(other.closingTimeNoncompetitive))
            return false;
        if (competitiveAccepted == null) {
            if (other.competitiveAccepted != null)
                return false;
        } else if (!competitiveAccepted.equals(other.competitiveAccepted))
            return false;
        if (competitiveBidDecimals == null) {
            if (other.competitiveBidDecimals != null)
                return false;
        } else if (!competitiveBidDecimals.equals(other.competitiveBidDecimals))
            return false;
        if (competitiveTendered == null) {
            if (other.competitiveTendered != null)
                return false;
        } else if (!competitiveTendered.equals(other.competitiveTendered))
            return false;
        if (competitiveTendersAccepted == null) {
            if (other.competitiveTendersAccepted != null)
                return false;
        } else if (!competitiveTendersAccepted.equals(other.competitiveTendersAccepted))
            return false;
        if (corpusCusip == null) {
            if (other.corpusCusip != null)
                return false;
        } else if (!corpusCusip.equals(other.corpusCusip))
            return false;
        if (cpiBaseReferencePeriod == null) {
            if (other.cpiBaseReferencePeriod != null)
                return false;
        } else if (!cpiBaseReferencePeriod.equals(other.cpiBaseReferencePeriod))
            return false;
        if (currentlyOutstanding == null) {
            if (other.currentlyOutstanding != null)
                return false;
        } else if (!currentlyOutstanding.equals(other.currentlyOutstanding))
            return false;
        if (cusip == null) {
            if (other.cusip != null)
                return false;
        } else if (!cusip.equals(other.cusip))
            return false;
        if (datedDate == null) {
            if (other.datedDate != null)
                return false;
        } else if (!datedDate.equals(other.datedDate))
            return false;
        if (directBidderAccepted == null) {
            if (other.directBidderAccepted != null)
                return false;
        } else if (!directBidderAccepted.equals(other.directBidderAccepted))
            return false;
        if (directBidderTendered == null) {
            if (other.directBidderTendered != null)
                return false;
        } else if (!directBidderTendered.equals(other.directBidderTendered))
            return false;
        if (estimatedAmountOfPubliclyHeldMaturingSecuritiesByType == null) {
            if (other.estimatedAmountOfPubliclyHeldMaturingSecuritiesByType != null)
                return false;
        } else if (!estimatedAmountOfPubliclyHeldMaturingSecuritiesByType
                .equals(other.estimatedAmountOfPubliclyHeldMaturingSecuritiesByType))
            return false;
        if (fimaIncluded == null) {
            if (other.fimaIncluded != null)
                return false;
        } else if (!fimaIncluded.equals(other.fimaIncluded))
            return false;
        if (fimaNoncompetitiveAccepted == null) {
            if (other.fimaNoncompetitiveAccepted != null)
                return false;
        } else if (!fimaNoncompetitiveAccepted.equals(other.fimaNoncompetitiveAccepted))
            return false;
        if (fimaNoncompetitiveTendered == null) {
            if (other.fimaNoncompetitiveTendered != null)
                return false;
        } else if (!fimaNoncompetitiveTendered.equals(other.fimaNoncompetitiveTendered))
            return false;
        if (firstInterestPaymentDate == null) {
            if (other.firstInterestPaymentDate != null)
                return false;
        } else if (!firstInterestPaymentDate.equals(other.firstInterestPaymentDate))
            return false;
        if (firstInterestPeriod == null) {
            if (other.firstInterestPeriod != null)
                return false;
        } else if (!firstInterestPeriod.equals(other.firstInterestPeriod))
            return false;
        if (floatingRate == null) {
            if (other.floatingRate != null)
                return false;
        } else if (!floatingRate.equals(other.floatingRate))
            return false;
        if (frnIndexDeterminationDate == null) {
            if (other.frnIndexDeterminationDate != null)
                return false;
        } else if (!frnIndexDeterminationDate.equals(other.frnIndexDeterminationDate))
            return false;
        if (frnIndexDeterminationRate == null) {
            if (other.frnIndexDeterminationRate != null)
                return false;
        } else if (!frnIndexDeterminationRate.equals(other.frnIndexDeterminationRate))
            return false;
        if (highDiscountMargin == null) {
            if (other.highDiscountMargin != null)
                return false;
        } else if (!highDiscountMargin.equals(other.highDiscountMargin))
            return false;
        if (highDiscountRate == null) {
            if (other.highDiscountRate != null)
                return false;
        } else if (!highDiscountRate.equals(other.highDiscountRate))
            return false;
        if (highInvestmentRate == null) {
            if (other.highInvestmentRate != null)
                return false;
        } else if (!highInvestmentRate.equals(other.highInvestmentRate))
            return false;
        if (highPrice == null) {
            if (other.highPrice != null)
                return false;
        } else if (!highPrice.equals(other.highPrice))
            return false;
        if (highYield == null) {
            if (other.highYield != null)
                return false;
        } else if (!highYield.equals(other.highYield))
            return false;
        if (indexRatioOnIssueDate == null) {
            if (other.indexRatioOnIssueDate != null)
                return false;
        } else if (!indexRatioOnIssueDate.equals(other.indexRatioOnIssueDate))
            return false;
        if (indirectBidderAccepted == null) {
            if (other.indirectBidderAccepted != null)
                return false;
        } else if (!indirectBidderAccepted.equals(other.indirectBidderAccepted))
            return false;
        if (indirectBidderTendered == null) {
            if (other.indirectBidderTendered != null)
                return false;
        } else if (!indirectBidderTendered.equals(other.indirectBidderTendered))
            return false;
        if (interestPaymentFrequency == null) {
            if (other.interestPaymentFrequency != null)
                return false;
        } else if (!interestPaymentFrequency.equals(other.interestPaymentFrequency))
            return false;
        if (interestRate == null) {
            if (other.interestRate != null)
                return false;
        } else if (!interestRate.equals(other.interestRate))
            return false;
        if (issueDate == null) {
            if (other.issueDate != null)
                return false;
        } else if (!issueDate.equals(other.issueDate))
            return false;
        if (lowDiscountMargin == null) {
            if (other.lowDiscountMargin != null)
                return false;
        } else if (!lowDiscountMargin.equals(other.lowDiscountMargin))
            return false;
        if (lowDiscountRate == null) {
            if (other.lowDiscountRate != null)
                return false;
        } else if (!lowDiscountRate.equals(other.lowDiscountRate))
            return false;
        if (lowInvestmentRate == null) {
            if (other.lowInvestmentRate != null)
                return false;
        } else if (!lowInvestmentRate.equals(other.lowInvestmentRate))
            return false;
        if (lowPrice == null) {
            if (other.lowPrice != null)
                return false;
        } else if (!lowPrice.equals(other.lowPrice))
            return false;
        if (lowYield == null) {
            if (other.lowYield != null)
                return false;
        } else if (!lowYield.equals(other.lowYield))
            return false;
        if (maturingDate == null) {
            if (other.maturingDate != null)
                return false;
        } else if (!maturingDate.equals(other.maturingDate))
            return false;
        if (maturityDate == null) {
            if (other.maturityDate != null)
                return false;
        } else if (!maturityDate.equals(other.maturityDate))
            return false;
        if (maximumCompetitiveAward == null) {
            if (other.maximumCompetitiveAward != null)
                return false;
        } else if (!maximumCompetitiveAward.equals(other.maximumCompetitiveAward))
            return false;
        if (maximumNoncompetitiveAward == null) {
            if (other.maximumNoncompetitiveAward != null)
                return false;
        } else if (!maximumNoncompetitiveAward.equals(other.maximumNoncompetitiveAward))
            return false;
        if (maximumSingleBid == null) {
            if (other.maximumSingleBid != null)
                return false;
        } else if (!maximumSingleBid.equals(other.maximumSingleBid))
            return false;
        if (minimumBidAmount == null) {
            if (other.minimumBidAmount != null)
                return false;
        } else if (!minimumBidAmount.equals(other.minimumBidAmount))
            return false;
        if (minimumStripAmount == null) {
            if (other.minimumStripAmount != null)
                return false;
        } else if (!minimumStripAmount.equals(other.minimumStripAmount))
            return false;
        if (minimumToIssue == null) {
            if (other.minimumToIssue != null)
                return false;
        } else if (!minimumToIssue.equals(other.minimumToIssue))
            return false;
        if (multiplesToBid == null) {
            if (other.multiplesToBid != null)
                return false;
        } else if (!multiplesToBid.equals(other.multiplesToBid))
            return false;
        if (multiplesToIssue == null) {
            if (other.multiplesToIssue != null)
                return false;
        } else if (!multiplesToIssue.equals(other.multiplesToIssue))
            return false;
        if (nlpExclusionAmount == null) {
            if (other.nlpExclusionAmount != null)
                return false;
        } else if (!nlpExclusionAmount.equals(other.nlpExclusionAmount))
            return false;
        if (nlpReportingThreshold == null) {
            if (other.nlpReportingThreshold != null)
                return false;
        } else if (!nlpReportingThreshold.equals(other.nlpReportingThreshold))
            return false;
        if (noncompetitiveAccepted == null) {
            if (other.noncompetitiveAccepted != null)
                return false;
        } else if (!noncompetitiveAccepted.equals(other.noncompetitiveAccepted))
            return false;
        if (noncompetitiveTendersAccepted == null) {
            if (other.noncompetitiveTendersAccepted != null)
                return false;
        } else if (!noncompetitiveTendersAccepted.equals(other.noncompetitiveTendersAccepted))
            return false;
        if (offeringAmount == null) {
            if (other.offeringAmount != null)
                return false;
        } else if (!offeringAmount.equals(other.offeringAmount))
            return false;
        if (originalCusip == null) {
            if (other.originalCusip != null)
                return false;
        } else if (!originalCusip.equals(other.originalCusip))
            return false;
        if (originalDatedDate == null) {
            if (other.originalDatedDate != null)
                return false;
        } else if (!originalDatedDate.equals(other.originalDatedDate))
            return false;
        if (originalIssueDate == null) {
            if (other.originalIssueDate != null)
                return false;
        } else if (!originalIssueDate.equals(other.originalIssueDate))
            return false;
        if (originalSecurityTerm == null) {
            if (other.originalSecurityTerm != null)
                return false;
        } else if (!originalSecurityTerm.equals(other.originalSecurityTerm))
            return false;
        if (pdfFilenameAnnouncement == null) {
            if (other.pdfFilenameAnnouncement != null)
                return false;
        } else if (!pdfFilenameAnnouncement.equals(other.pdfFilenameAnnouncement))
            return false;
        if (pdfFilenameCompetitiveResults == null) {
            if (other.pdfFilenameCompetitiveResults != null)
                return false;
        } else if (!pdfFilenameCompetitiveResults.equals(other.pdfFilenameCompetitiveResults))
            return false;
        if (pdfFilenameNoncompetitiveResults == null) {
            if (other.pdfFilenameNoncompetitiveResults != null)
                return false;
        } else if (!pdfFilenameNoncompetitiveResults.equals(other.pdfFilenameNoncompetitiveResults))
            return false;
        if (pdfFilenameSpecialAnnouncement == null) {
            if (other.pdfFilenameSpecialAnnouncement != null)
                return false;
        } else if (!pdfFilenameSpecialAnnouncement.equals(other.pdfFilenameSpecialAnnouncement))
            return false;
        if (pricePer100 == null) {
            if (other.pricePer100 != null)
                return false;
        } else if (!pricePer100.equals(other.pricePer100))
            return false;
        if (primaryDealerAccepted == null) {
            if (other.primaryDealerAccepted != null)
                return false;
        } else if (!primaryDealerAccepted.equals(other.primaryDealerAccepted))
            return false;
        if (primaryDealerTendered == null) {
            if (other.primaryDealerTendered != null)
                return false;
        } else if (!primaryDealerTendered.equals(other.primaryDealerTendered))
            return false;
        if (refCpiOnDatedDate == null) {
            if (other.refCpiOnDatedDate != null)
                return false;
        } else if (!refCpiOnDatedDate.equals(other.refCpiOnDatedDate))
            return false;
        if (refCpiOnIssueDate == null) {
            if (other.refCpiOnIssueDate != null)
                return false;
        } else if (!refCpiOnIssueDate.equals(other.refCpiOnIssueDate))
            return false;
        if (reopening == null) {
            if (other.reopening != null)
                return false;
        } else if (!reopening.equals(other.reopening))
            return false;
        if (securityTerm == null) {
            if (other.securityTerm != null)
                return false;
        } else if (!securityTerm.equals(other.securityTerm))
            return false;
        if (securityTermDayMonth == null) {
            if (other.securityTermDayMonth != null)
                return false;
        } else if (!securityTermDayMonth.equals(other.securityTermDayMonth))
            return false;
        if (securityTermWeekYear == null) {
            if (other.securityTermWeekYear != null)
                return false;
        } else if (!securityTermWeekYear.equals(other.securityTermWeekYear))
            return false;
        if (securityType == null) {
            if (other.securityType != null)
                return false;
        } else if (!securityType.equals(other.securityType))
            return false;
        if (series == null) {
            if (other.series != null)
                return false;
        } else if (!series.equals(other.series))
            return false;
        if (somaAccepted == null) {
            if (other.somaAccepted != null)
                return false;
        } else if (!somaAccepted.equals(other.somaAccepted))
            return false;
        if (somaHoldings == null) {
            if (other.somaHoldings != null)
                return false;
        } else if (!somaHoldings.equals(other.somaHoldings))
            return false;
        if (somaIncluded == null) {
            if (other.somaIncluded != null)
                return false;
        } else if (!somaIncluded.equals(other.somaIncluded))
            return false;
        if (somaTendered == null) {
            if (other.somaTendered != null)
                return false;
        } else if (!somaTendered.equals(other.somaTendered))
            return false;
        if (spread == null) {
            if (other.spread != null)
                return false;
        } else if (!spread.equals(other.spread))
            return false;
        if (standardInterestPaymentPer1000 == null) {
            if (other.standardInterestPaymentPer1000 != null)
                return false;
        } else if (!standardInterestPaymentPer1000.equals(other.standardInterestPaymentPer1000))
            return false;
        if (strippable == null) {
            if (other.strippable != null)
                return false;
        } else if (!strippable.equals(other.strippable))
            return false;
        if (term == null) {
            if (other.term != null)
                return false;
        } else if (!term.equals(other.term))
            return false;
        if (tiinConversionFactorPer1000 == null) {
            if (other.tiinConversionFactorPer1000 != null)
                return false;
        } else if (!tiinConversionFactorPer1000.equals(other.tiinConversionFactorPer1000))
            return false;
        if (tips == null) {
            if (other.tips != null)
                return false;
        } else if (!tips.equals(other.tips))
            return false;
        if (totalAccepted == null) {
            if (other.totalAccepted != null)
                return false;
        } else if (!totalAccepted.equals(other.totalAccepted))
            return false;
        if (totalTendered == null) {
            if (other.totalTendered != null)
                return false;
        } else if (!totalTendered.equals(other.totalTendered))
            return false;
        if (treasuryDirectAccepted == null) {
            if (other.treasuryDirectAccepted != null)
                return false;
        } else if (!treasuryDirectAccepted.equals(other.treasuryDirectAccepted))
            return false;
        if (treasuryDirectTendersAccepted == null) {
            if (other.treasuryDirectTendersAccepted != null)
                return false;
        } else if (!treasuryDirectTendersAccepted.equals(other.treasuryDirectTendersAccepted))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (unadjustedAccruedInterestPer1000 == null) {
            if (other.unadjustedAccruedInterestPer1000 != null)
                return false;
        } else if (!unadjustedAccruedInterestPer1000.equals(other.unadjustedAccruedInterestPer1000))
            return false;
        if (unadjustedPrice == null) {
            if (other.unadjustedPrice != null)
                return false;
        } else if (!unadjustedPrice.equals(other.unadjustedPrice))
            return false;
        if (updatedTimestamp == null) {
            if (other.updatedTimestamp != null)
                return false;
        } else if (!updatedTimestamp.equals(other.updatedTimestamp))
            return false;
        if (xmlFilenameAnnouncement == null) {
            if (other.xmlFilenameAnnouncement != null)
                return false;
        } else if (!xmlFilenameAnnouncement.equals(other.xmlFilenameAnnouncement))
            return false;
        if (xmlFilenameCompetitiveResults == null) {
            if (other.xmlFilenameCompetitiveResults != null)
                return false;
        } else if (!xmlFilenameCompetitiveResults.equals(other.xmlFilenameCompetitiveResults))
            return false;
        if (xmlFilenameSpecialAnnouncement == null) {
            if (other.xmlFilenameSpecialAnnouncement != null)
                return false;
        } else if (!xmlFilenameSpecialAnnouncement.equals(other.xmlFilenameSpecialAnnouncement))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Security [cusip=" + cusip + ", issueDate=" + issueDate + ", securityType=" + securityType
            + ", securityTerm=" + securityTerm + ", maturityDate=" + maturityDate + ", interestRate=" + interestRate
            + ", refCpiOnIssueDate=" + refCpiOnIssueDate + ", refCpiOnDatedDate=" + refCpiOnDatedDate
            + ", announcementDate=" + announcementDate + ", auctionDate=" + auctionDate + ", auctionDateYear="
            + auctionDateYear + ", datedDate=" + datedDate + ", accruedInterestPer1000=" + accruedInterestPer1000
            + ", accruedInterestPer100=" + accruedInterestPer100 + ", adjustedAccruedInterestPer1000="
            + adjustedAccruedInterestPer1000 + ", adjustedPrice=" + adjustedPrice + ", allocationPercentage="
            + allocationPercentage + ", allocationPercentageDecimals=" + allocationPercentageDecimals
            + ", announcedCusip=" + announcedCusip + ", auctionFormat=" + auctionFormat
            + ", averageMedianDiscountRate=" + averageMedianDiscountRate + ", averageMedianInvestmentRate="
            + averageMedianInvestmentRate + ", averageMedianPrice=" + averageMedianPrice
            + ", averageMedianDiscountMargin=" + averageMedianDiscountMargin + ", averageMedianYield="
            + averageMedianYield + ", backDated=" + backDated + ", backDatedDate=" + backDatedDate
            + ", bidToCoverRatio=" + bidToCoverRatio + ", callDate=" + callDate + ", callable=" + callable
            + ", calledDate=" + calledDate + ", cashManagementBillCMB=" + cashManagementBillCMB
            + ", closingTimeCompetitive=" + closingTimeCompetitive + ", closingTimeNoncompetitive="
            + closingTimeNoncompetitive + ", competitiveAccepted=" + competitiveAccepted
            + ", competitiveBidDecimals=" + competitiveBidDecimals + ", competitiveTendered=" + competitiveTendered
            + ", competitiveTendersAccepted=" + competitiveTendersAccepted + ", corpusCusip=" + corpusCusip
            + ", cpiBaseReferencePeriod=" + cpiBaseReferencePeriod + ", currentlyOutstanding="
            + currentlyOutstanding + ", directBidderAccepted=" + directBidderAccepted + ", directBidderTendered="
            + directBidderTendered + ", estimatedAmountOfPubliclyHeldMaturingSecuritiesByType="
            + estimatedAmountOfPubliclyHeldMaturingSecuritiesByType + ", fimaIncluded=" + fimaIncluded
            + ", fimaNoncompetitiveAccepted=" + fimaNoncompetitiveAccepted + ", fimaNoncompetitiveTendered="
            + fimaNoncompetitiveTendered + ", firstInterestPeriod=" + firstInterestPeriod
            + ", firstInterestPaymentDate=" + firstInterestPaymentDate + ", floatingRate=" + floatingRate
            + ", frnIndexDeterminationDate=" + frnIndexDeterminationDate + ", frnIndexDeterminationRate="
            + frnIndexDeterminationRate + ", highDiscountRate=" + highDiscountRate + ", highInvestmentRate="
            + highInvestmentRate + ", highPrice=" + highPrice + ", highDiscountMargin=" + highDiscountMargin
            + ", highYield=" + highYield + ", indexRatioOnIssueDate=" + indexRatioOnIssueDate
            + ", indirectBidderAccepted=" + indirectBidderAccepted + ", indirectBidderTendered="
            + indirectBidderTendered + ", interestPaymentFrequency=" + interestPaymentFrequency
            + ", lowDiscountRate=" + lowDiscountRate + ", lowInvestmentRate=" + lowInvestmentRate + ", lowPrice="
            + lowPrice + ", lowDiscountMargin=" + lowDiscountMargin + ", lowYield=" + lowYield + ", maturingDate="
            + maturingDate + ", maximumCompetitiveAward=" + maximumCompetitiveAward
            + ", maximumNoncompetitiveAward=" + maximumNoncompetitiveAward + ", maximumSingleBid="
            + maximumSingleBid + ", minimumBidAmount=" + minimumBidAmount + ", minimumStripAmount="
            + minimumStripAmount + ", minimumToIssue=" + minimumToIssue + ", multiplesToBid=" + multiplesToBid
            + ", multiplesToIssue=" + multiplesToIssue + ", nlpExclusionAmount=" + nlpExclusionAmount
            + ", nlpReportingThreshold=" + nlpReportingThreshold + ", noncompetitiveAccepted="
            + noncompetitiveAccepted + ", noncompetitiveTendersAccepted=" + noncompetitiveTendersAccepted
            + ", offeringAmount=" + offeringAmount + ", originalCusip=" + originalCusip + ", originalDatedDate="
            + originalDatedDate + ", originalIssueDate=" + originalIssueDate + ", originalSecurityTerm="
            + originalSecurityTerm + ", pdfFilenameAnnouncement=" + pdfFilenameAnnouncement
            + ", pdfFilenameCompetitiveResults=" + pdfFilenameCompetitiveResults
            + ", pdfFilenameNoncompetitiveResults=" + pdfFilenameNoncompetitiveResults
            + ", pdfFilenameSpecialAnnouncement=" + pdfFilenameSpecialAnnouncement + ", pricePer100=" + pricePer100
            + ", primaryDealerAccepted=" + primaryDealerAccepted + ", primaryDealerTendered="
            + primaryDealerTendered + ", reopening=" + reopening + ", securityTermDayMonth=" + securityTermDayMonth
            + ", securityTermWeekYear=" + securityTermWeekYear + ", series=" + series + ", somaAccepted="
            + somaAccepted + ", somaHoldings=" + somaHoldings + ", somaIncluded=" + somaIncluded + ", somaTendered="
            + somaTendered + ", spread=" + spread + ", standardInterestPaymentPer1000="
            + standardInterestPaymentPer1000 + ", strippable=" + strippable + ", term=" + term
            + ", tiinConversionFactorPer1000=" + tiinConversionFactorPer1000 + ", tips=" + tips + ", totalAccepted="
            + totalAccepted + ", totalTendered=" + totalTendered + ", treasuryDirectAccepted="
            + treasuryDirectAccepted + ", treasuryDirectTendersAccepted=" + treasuryDirectTendersAccepted
            + ", type=" + type + ", unadjustedAccruedInterestPer1000=" + unadjustedAccruedInterestPer1000
            + ", unadjustedPrice=" + unadjustedPrice + ", updatedTimestamp=" + updatedTimestamp
            + ", xmlFilenameAnnouncement=" + xmlFilenameAnnouncement + ", xmlFilenameCompetitiveResults="
            + xmlFilenameCompetitiveResults + ", xmlFilenameSpecialAnnouncement=" + xmlFilenameSpecialAnnouncement
            + ", getCreatedDate()=" + getCreatedDate() + ", getUpdatedDate()=" + getUpdatedDate()
            + ", getVersion()=" + getVersion() + ", getPrimaryKey()=" + getPrimaryKey() + "]";
    }
}
