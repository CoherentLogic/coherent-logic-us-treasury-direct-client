package com.coherentlogic.treasurydirect.client.core.adapters;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.exceptions.ConversionFailedException;
import com.coherentlogic.coherent.data.adapter.core.exceptions.MethodNotSupportedException;
import com.coherentlogic.coherent.data.adapter.core.factories.TypedFactory;
import com.coherentlogic.treasurydirect.client.core.domain.Securities;
import com.coherentlogic.treasurydirect.client.core.domain.Security;
import com.coherentlogic.treasurydirect.client.core.exceptions.DateConversionFailedRuntimeException;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class SecuritiesAdapter extends AbstractGSONBasedTypeAdapter<Securities> {

    private static final Logger log = LoggerFactory.getLogger(SecuritiesAdapter.class);

    public static final String BEAN_NAME = "securitiesAdapter";

    final DateFormat DATE_T_TIME_DATE_FORMAT = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ss");

    final DateFormat TIME_ONLY_FORMAT = new SimpleDateFormat ("hh:mm a");

    public SecuritiesAdapter(TypedFactory<Securities> typedFactory) {
        super(typedFactory);
    }

    public SecuritiesAdapter(GsonBuilder gsonBuilder, TypedFactory<Securities> typedFactory) {
        super(gsonBuilder, typedFactory);
    }

    @Override
    public Securities read(JsonReader reader) throws IOException {

        log.debug("read: method begins; reader: " + reader);

        Securities result = new Securities ();

        JsonElement securitiesElement = null;

        JsonToken token = reader.peek ();

        if (JsonToken.BEGIN_ARRAY.equals(token)) {
            securitiesElement = getGsonBuilder().create().fromJson(reader, JsonArray.class);
        } else if (JsonToken.BEGIN_OBJECT.equals(token)) {
            securitiesElement = getGsonBuilder().create().fromJson(reader, JsonObject.class);
        } else {
            throw new ConversionFailedException("Unable to convert the JSON into either an array or an object due to "
                + "an unsupported token type (token: " + token + ").");
        }

        if (securitiesElement.isJsonArray()) {

            JsonArray securityArray = securitiesElement.getAsJsonArray();

            List<Security> securityList = asSecurityList (securityArray);

            result.setSecurityList(securityList);

        } else {

            List<Security> securityList = new ArrayList<Security> ();

            Security security = asSecurity(securitiesElement.getAsJsonObject());

            securityList.add(security);

            result.setSecurityList(securityList);
        }

        log.debug("read: method ends; result: " + result);

        return result;
    }

    /**
     * 
{"cusip":"912796CJ6"
"issueDate":"2014-02-11T00:00:00"
"securityType":"Bill"
"securityTerm":"72-Day"
"maturityDate":"2014-04-24T00:00:00"
"interestRate":""
"refCpiOnIssueDate":""
"refCpiOnDatedDate":""
"announcementDate":"2014-02-06T00:00:00"
"auctionDate":"2014-02-10T00:00:00"
"auctionDateYear":"2014"
"datedDate":""
"accruedInterestPer1000":""
"accruedInterestPer100":""
"adjustedAccruedInterestPer1000":""
"adjustedPrice":""
"allocationPercentage":"99.620000"
"allocationPercentageDecimals":"2"
"announcedCusip":""
"auctionFormat":"Single-Price"
"averageMedianDiscountRate":"0.080000"
"averageMedianInvestmentRate":""
"averageMedianPrice":""
"averageMedianDiscountMargin":""
"averageMedianYield":""
"backDated":""
"backDatedDate":""
"bidToCoverRatio":"3.380000"
"callDate":""
"callable":""
"calledDate":""
"cashManagementBillCMB":"Yes"
"closingTimeCompetitive":"01:00 PM"
"closingTimeNoncompetitive":"12:00 PM"
"competitiveAccepted":"49995325000"
"competitiveBidDecimals":"3"
"competitiveTendered":"169055000000"
"competitiveTendersAccepted":"Yes"
"corpusCusip":""
"cpiBaseReferencePeriod":""
"currentlyOutstanding":"84002000000.000000"
"directBidderAccepted":"4763635000"
"directBidderTendered":"15545000000"
"estimatedAmountOfPubliclyHeldMaturingSecuritiesByType":""
"fimaIncluded":"Yes"
"fimaNoncompetitiveAccepted":"0"
"fimaNoncompetitiveTendered":"0"
"firstInterestPeriod":""
"firstInterestPaymentDate":""
"floatingRate":"No"
"frnIndexDeterminationDate":""
"frnIndexDeterminationRate":""
"highDiscountRate":"0.090000"
"highInvestmentRate":"0.091000"
"highPrice":"99.982000"
"highDiscountMargin":""
"highYield":""
"indexRatioOnIssueDate":""
"indirectBidderAccepted":"5495180000"
"indirectBidderTendered":"7660000000"
"interestPaymentFrequency":"None"
"lowDiscountRate":"0.050000"
"lowInvestmentRate":""
"lowPrice":""
"lowDiscountMargin":""
"lowYield":""
"maturingDate":"2014-02-11T00:00:00"
"maximumCompetitiveAward":"17500000000"
"maximumNoncompetitiveAward":"5000000"
"maximumSingleBid":"17500000000"
"minimumBidAmount":"100"
"minimumStripAmount":""
"minimumToIssue":"100"
"multiplesToBid":"100"
"multiplesToIssue":"100"
"nlpExclusionAmount":"29500000000"
"nlpReportingThreshold":"17500000000"
"noncompetitiveAccepted":"4960000"
"noncompetitiveTendersAccepted":"Yes"
"offeringAmount":"50000000000"
"originalCusip":""
"originalDatedDate":""
"originalIssueDate":"2013-10-17T00:00:00"
"originalSecurityTerm":"189-Day"
"pdfFilenameAnnouncement":"A_20140206_2.pdf"
"pdfFilenameCompetitiveResults":"R_20140210_3.pdf"
"pdfFilenameNoncompetitiveResults":"NCR_20140210_3.pdf"
"pdfFilenameSpecialAnnouncement":""
"pricePer100":"99.982000"
"primaryDealerAccepted":"39736510000"
"primaryDealerTendered":"145850000000"
"reopening":"Yes"
"securityTermDayMonth":"72-Day"
"securityTermWeekYear":"0-Week"
"series":""
"somaAccepted":"0"
"somaHoldings":"0"
"somaIncluded":"No"
"somaTendered":"0"
"spread":""
"standardInterestPaymentPer1000":""
"strippable":""
"term":""
"tiinConversionFactorPer1000":""
"tips":"No"
"totalAccepted":"50000285000"
"totalTendered":"169059960000"
"treasuryDirectAccepted":"0"
"treasuryDirectTendersAccepted":"No"
"type":"CMB"
"unadjustedAccruedInterestPer1000":""
"unadjustedPrice":""
"updatedTimestamp":"2014-03-07T12:05:06"
"xmlFilenameAnnouncement":"A_20140206_2.xml"
"xmlFilenameCompetitiveResults":"R_20140210_3.xml"
"xmlFilenameSpecialAnnouncement":""}
     */
    Security asSecurity (JsonObject securityObject) {

        log.debug("asSecurity: method begins; securityObject: " + securityObject);

        Security result = new Security ();

        if (!securityObject.has("cusip"))
            throw new ConversionFailedException("The JSON does not contain a member with name cusip");

        String cusip = securityObject.get("cusip").getAsString();

        log.debug("cusip: " + cusip);

        result.setCusip(cusip);

        if (!securityObject.has("issueDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name issueDate");

        JsonElement issueDate = securityObject.get("issueDate");

        log.debug("issueDate: " + issueDate);

            result.setIssueDate(asDate ("issueDate", issueDate));

        if (!securityObject.has("securityType"))
            throw new ConversionFailedException("The JSON does not contain a member with name securityType");

        String securityType = securityObject.get("securityType").getAsString();

        log.debug("securityType: " + securityType);

        result.setSecurityType(securityType);

        if (!securityObject.has("securityTerm"))
            throw new ConversionFailedException("The JSON does not contain a member with name securityTerm");

        String securityTerm = securityObject.get("securityTerm").getAsString();

        log.debug("securityTerm: " + securityTerm);

        result.setSecurityTerm(securityTerm);

        if (!securityObject.has("maturityDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name maturityDate");

        JsonElement maturityDate = securityObject.get("maturityDate");

        log.debug("maturityDate: " + maturityDate);

        result.setMaturityDate(asDate ("maturityDate", maturityDate));

        if (!securityObject.has("interestRate"))
            throw new ConversionFailedException("The JSON does not contain a member with name interestRate");

        BigDecimal interestRate = asBigDecimal (securityObject.get("interestRate"));//.getAsBigDecimal();

        log.debug("interestRate: " + interestRate);

        result.setInterestRate(interestRate);

        if (!securityObject.has("refCpiOnIssueDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name refCpiOnIssueDate");

        BigDecimal refCpiOnIssueDate = asBigDecimal (securityObject.get("refCpiOnIssueDate"));//.getAsBigDecimal();

        log.debug("refCpiOnIssueDate: " + refCpiOnIssueDate);

        result.setRefCpiOnIssueDate(refCpiOnIssueDate);

        if (!securityObject.has("refCpiOnDatedDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name refCpiOnDatedDate");

        BigDecimal refCpiOnDatedDate = asBigDecimal (securityObject.get("refCpiOnDatedDate"));//.getAsBigDecimal();

        log.debug("refCpiOnDatedDate: " + refCpiOnDatedDate);

        result.setRefCpiOnDatedDate(refCpiOnDatedDate);

        if (!securityObject.has("announcementDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name announcementDate");

        Date announcementDate = asDate ("announcementDate", securityObject.get("announcementDate"));

        log.debug("announcementDate: " + announcementDate);

        result.setAnnouncementDate(announcementDate);

        if (!securityObject.has("auctionDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name auctionDate");

        Date auctionDate = asDate ("auctionDate", securityObject.get("auctionDate"));//.getAsString();

        log.debug("auctionDate: " + auctionDate);

        result.setAuctionDate(auctionDate);

        if (!securityObject.has("auctionDateYear"))
            throw new ConversionFailedException("The JSON does not contain a member with name auctionDateYear");

        String auctionDateYear = securityObject.get("auctionDateYear").getAsString();

        log.debug("auctionDateYear: " + auctionDateYear);

        result.setAuctionDateYear(auctionDateYear);

        if (!securityObject.has("datedDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name datedDate");

        Date datedDate = asDate ("datedDate", securityObject.get("datedDate"));

        log.debug("datedDate: " + datedDate);

        result.setDatedDate(datedDate);

        if (!securityObject.has("accruedInterestPer1000"))
            throw new ConversionFailedException("The JSON does not contain a member with name accruedInterestPer1000");

        BigDecimal accruedInterestPer1000 = asBigDecimal (securityObject.get("accruedInterestPer1000"));//.getAsBigDecimal();

        log.debug("accruedInterestPer1000: " + accruedInterestPer1000);

        result.setAccruedInterestPer1000(accruedInterestPer1000);

        if (!securityObject.has("accruedInterestPer100"))
            throw new ConversionFailedException("The JSON does not contain a member with name accruedInterestPer100");

        BigDecimal accruedInterestPer100 = asBigDecimal (securityObject.get("accruedInterestPer100"));//.getAsBigDecimal();

        log.debug("accruedInterestPer100: " + accruedInterestPer100);

        result.setAccruedInterestPer100(accruedInterestPer100);

        if (!securityObject.has("adjustedAccruedInterestPer1000"))
            throw new ConversionFailedException("The JSON does not contain a member with name adjustedAccruedInterestPer1000");

        BigDecimal adjustedAccruedInterestPer1000 = asBigDecimal (securityObject.get("adjustedAccruedInterestPer1000"));//.getAsBigDecimal();

        log.debug("adjustedAccruedInterestPer1000: " + adjustedAccruedInterestPer1000);

        result.setAdjustedAccruedInterestPer1000(adjustedAccruedInterestPer1000);

        if (!securityObject.has("adjustedPrice"))
            throw new ConversionFailedException("The JSON does not contain a member with name adjustedPrice");

        BigDecimal adjustedPrice = asBigDecimal (securityObject.get("adjustedPrice"));//.getAsBigDecimal();

        log.debug("adjustedPrice: " + adjustedPrice);

        result.setAdjustedPrice(adjustedPrice);

        if (!securityObject.has("allocationPercentage"))
            throw new ConversionFailedException("The JSON does not contain a member with name allocationPercentage");

        BigDecimal allocationPercentage = asBigDecimal (securityObject.get("allocationPercentage"));//.getAsBigDecimal();

        log.debug("allocationPercentage: " + allocationPercentage);

        result.setAllocationPercentage(allocationPercentage);

        if (!securityObject.has("allocationPercentageDecimals"))
            throw new ConversionFailedException("The JSON does not contain a member with name allocationPercentageDecimals");

        Integer allocationPercentageDecimals = asInteger (securityObject.get("allocationPercentageDecimals"));

        log.debug("allocationPercentageDecimals: " + allocationPercentageDecimals);

        result.setAllocationPercentageDecimals(allocationPercentageDecimals);

        if (!securityObject.has("announcedCusip"))
            throw new ConversionFailedException("The JSON does not contain a member with name announcedCusip");

        String announcedCusip = securityObject.get("announcedCusip").getAsString();

        log.debug("announcedCusip: " + announcedCusip);

        result.setAnnouncedCusip(announcedCusip);

        if (!securityObject.has("auctionFormat"))
            throw new ConversionFailedException("The JSON does not contain a member with name auctionFormat");

        String auctionFormat = securityObject.get("auctionFormat").getAsString();

        log.debug("auctionFormat: " + auctionFormat);

        result.setAuctionFormat(auctionFormat);

        if (!securityObject.has("averageMedianDiscountRate"))
            throw new ConversionFailedException("The JSON does not contain a member with name averageMedianDiscountRate");

        BigDecimal averageMedianDiscountRate = asBigDecimal (securityObject.get("averageMedianDiscountRate"));//.getAsBigDecimal();

        log.debug("averageMedianDiscountRate: " + averageMedianDiscountRate);

        result.setAverageMedianDiscountRate(averageMedianDiscountRate);

        if (!securityObject.has("averageMedianInvestmentRate"))
            throw new ConversionFailedException("The JSON does not contain a member with name averageMedianInvestmentRate");

        BigDecimal averageMedianInvestmentRate = asBigDecimal (securityObject.get("averageMedianInvestmentRate"));//.getAsBigDecimal();

        log.debug("averageMedianInvestmentRate: " + averageMedianInvestmentRate);

        result.setAverageMedianInvestmentRate(averageMedianInvestmentRate);

        if (!securityObject.has("averageMedianPrice"))
            throw new ConversionFailedException("The JSON does not contain a member with name averageMedianPrice");

        BigDecimal averageMedianPrice = asBigDecimal (securityObject.get("averageMedianPrice"));//.getAsBigDecimal();

        log.debug("averageMedianPrice: " + averageMedianPrice);

        result.setAverageMedianPrice(averageMedianPrice);

        if (!securityObject.has("averageMedianDiscountMargin"))
            throw new ConversionFailedException("The JSON does not contain a member with name averageMedianDiscountMargin");

        BigDecimal averageMedianDiscountMargin = asBigDecimal (securityObject.get("averageMedianDiscountMargin"));//.getAsBigDecimal();

        log.debug("averageMedianDiscountMargin: " + averageMedianDiscountMargin);

        result.setAverageMedianDiscountMargin(averageMedianDiscountMargin);

        if (!securityObject.has("averageMedianYield"))
            throw new ConversionFailedException("The JSON does not contain a member with name averageMedianYield");

        BigDecimal averageMedianYield = asBigDecimal (securityObject.get("averageMedianYield"));//.getAsBigDecimal();

        log.debug("averageMedianYield: " + averageMedianYield);

        result.setAverageMedianYield(averageMedianYield);

        if (!securityObject.has("backDated"))
            throw new ConversionFailedException("The JSON does not contain a member with name backDated");

        String backDated = securityObject.get("backDated").getAsString();

        log.debug("backDated: " + backDated);

        result.setBackDated(asBoolean (backDated));

        if (!securityObject.has("backDatedDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name backDatedDate");

        /*
         * "backDatedDate":"2018-01-15T00:00:00"
         */
        Date backDatedDate = asDate("backDatedDate", securityObject.get("backDatedDate"));

        log.debug("backDatedDate: " + backDatedDate);

        result.setBackDatedDate(backDatedDate);

        if (!securityObject.has("bidToCoverRatio"))
            throw new ConversionFailedException("The JSON does not contain a member with name bidToCoverRatio");

        BigDecimal bidToCoverRatio = asBigDecimal (securityObject.get("bidToCoverRatio"));//.getAsBigDecimal();

        log.debug("bidToCoverRatio: " + bidToCoverRatio);

        result.setBidToCoverRatio(bidToCoverRatio);

        if (!securityObject.has("callDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name callDate");

        String callDate = securityObject.get("callDate").getAsString();

        log.debug("callDate: " + callDate);

        result.setCallDate(callDate);

        if (!securityObject.has("callable"))
            throw new ConversionFailedException("The JSON does not contain a member with name callable");

        String callable = securityObject.get("callable").getAsString();

        log.debug("callable: " + callable);

        result.setCallable(asBoolean (callable));

        if (!securityObject.has("calledDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name calledDate");

        String calledDate = securityObject.get("calledDate").getAsString();

        log.debug("calledDate: " + calledDate);

        result.setCalledDate(calledDate);

        if (!securityObject.has("cashManagementBillCMB"))
            throw new ConversionFailedException("The JSON does not contain a member with name cashManagementBillCMB");

        String cashManagementBillCMB = securityObject.get("cashManagementBillCMB").getAsString();

        log.debug("cashManagementBillCMB: " + cashManagementBillCMB);

        result.setCashManagementBillCMB(asBoolean (cashManagementBillCMB));

        if (!securityObject.has("closingTimeCompetitive"))
            throw new ConversionFailedException("The JSON does not contain a member with name closingTimeCompetitive");

        Date closingTimeCompetitive = asTime("closingTimeCompetitive", securityObject.get("closingTimeCompetitive"));

        log.debug("closingTimeCompetitive: " + closingTimeCompetitive);

        result.setClosingTimeCompetitive(closingTimeCompetitive);

        if (!securityObject.has("closingTimeNoncompetitive"))
            throw new ConversionFailedException("The JSON does not contain a member with name closingTimeNoncompetitive");

        Date closingTimeNoncompetitive = asTime ("closingTimeNoncompetitive", securityObject.get("closingTimeNoncompetitive"));

        log.debug("closingTimeNoncompetitive: " + closingTimeNoncompetitive);

        result.setClosingTimeNoncompetitive(closingTimeNoncompetitive);

        if (!securityObject.has("competitiveAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name competitiveAccepted");

        Long competitiveAccepted = asLong (securityObject.get("competitiveAccepted"));

        log.debug("competitiveAccepted: " + competitiveAccepted);

        result.setCompetitiveAccepted(competitiveAccepted);

        if (!securityObject.has("competitiveBidDecimals"))
            throw new ConversionFailedException("The JSON does not contain a member with name competitiveBidDecimals");

        Integer competitiveBidDecimals = asInteger (securityObject.get("competitiveBidDecimals"));

        log.debug("competitiveBidDecimals: " + competitiveBidDecimals);

        result.setCompetitiveBidDecimals(competitiveBidDecimals);

        if (!securityObject.has("competitiveTendered"))
            throw new ConversionFailedException("The JSON does not contain a member with name competitiveTendered");

        Long competitiveTendered = asLong (securityObject.get("competitiveTendered"));

        log.debug("competitiveTendered: " + competitiveTendered);

        result.setCompetitiveTendered(competitiveTendered);

        if (!securityObject.has("competitiveTendersAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name competitiveTendersAccepted");

        String competitiveTendersAccepted = securityObject.get("competitiveTendersAccepted").getAsString();

        log.debug("competitiveTendersAccepted: " + competitiveTendersAccepted);

        result.setCompetitiveTendersAccepted(asBoolean (competitiveTendersAccepted));

        if (!securityObject.has("corpusCusip"))
            throw new ConversionFailedException("The JSON does not contain a member with name corpusCusip");

        String corpusCusip = securityObject.get("corpusCusip").getAsString();

        log.debug("corpusCusip: " + corpusCusip);

        result.setCorpusCusip(corpusCusip);

        if (!securityObject.has("cpiBaseReferencePeriod"))
            throw new ConversionFailedException("The JSON does not contain a member with name cpiBaseReferencePeriod");

        String cpiBaseReferencePeriod = securityObject.get("cpiBaseReferencePeriod").getAsString();

        log.debug("cpiBaseReferencePeriod: " + cpiBaseReferencePeriod);

        result.setCpiBaseReferencePeriod(cpiBaseReferencePeriod);

        if (!securityObject.has("currentlyOutstanding"))
            throw new ConversionFailedException("The JSON does not contain a member with name currentlyOutstanding");

        String currentlyOutstanding = securityObject.get("currentlyOutstanding").getAsString();

        log.debug("currentlyOutstanding: " + currentlyOutstanding);

        result.setCurrentlyOutstanding(currentlyOutstanding);

        if (!securityObject.has("directBidderAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name directBidderAccepted");

        Long directBidderAccepted = asLong (securityObject.get("directBidderAccepted"));

        log.debug("directBidderAccepted: " + directBidderAccepted);

        result.setDirectBidderAccepted(directBidderAccepted);

        if (!securityObject.has("directBidderTendered"))
            throw new ConversionFailedException("The JSON does not contain a member with name directBidderTendered");

        Long directBidderTendered = asLong (securityObject.get("directBidderTendered"));

        log.debug("directBidderTendered: " + directBidderTendered);

        result.setDirectBidderTendered(directBidderTendered);

        if (!securityObject.has("estimatedAmountOfPubliclyHeldMaturingSecuritiesByType"))
            throw new ConversionFailedException("The JSON does not contain a member with name estimatedAmountOfPubliclyHeldMaturingSecuritiesByType");

        String estimatedAmountOfPubliclyHeldMaturingSecuritiesByType = securityObject.get("estimatedAmountOfPubliclyHeldMaturingSecuritiesByType").getAsString();

        log.debug("estimatedAmountOfPubliclyHeldMaturingSecuritiesByType: " + estimatedAmountOfPubliclyHeldMaturingSecuritiesByType);

        result.setEstimatedAmountOfPubliclyHeldMaturingSecuritiesByType(estimatedAmountOfPubliclyHeldMaturingSecuritiesByType);

        if (!securityObject.has("fimaIncluded"))
            throw new ConversionFailedException("The JSON does not contain a member with name fimaIncluded");

        String fimaIncluded = securityObject.get("fimaIncluded").getAsString();

        log.debug("fimaIncluded: " + fimaIncluded);

        result.setFimaIncluded(asBoolean (fimaIncluded));

        if (!securityObject.has("fimaNoncompetitiveAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name fimaNoncompetitiveAccepted");

        BigDecimal fimaNoncompetitiveAccepted = asBigDecimal (securityObject.get("fimaNoncompetitiveAccepted"));//getAsBigDecimal();

        log.debug("fimaNoncompetitiveAccepted: " + fimaNoncompetitiveAccepted);

        result.setFimaNoncompetitiveAccepted(fimaNoncompetitiveAccepted);

        if (!securityObject.has("fimaNoncompetitiveTendered"))
            throw new ConversionFailedException("The JSON does not contain a member with name fimaNoncompetitiveTendered");

        BigDecimal fimaNoncompetitiveTendered = asBigDecimal (securityObject.get("fimaNoncompetitiveTendered"));//.getAsBigDecimal();

        log.debug("fimaNoncompetitiveTendered: " + fimaNoncompetitiveTendered);

        result.setFimaNoncompetitiveTendered(fimaNoncompetitiveTendered);

        if (!securityObject.has("firstInterestPeriod"))
            throw new ConversionFailedException("The JSON does not contain a member with name firstInterestPeriod");

        String firstInterestPeriod = securityObject.get("firstInterestPeriod").getAsString();

        log.debug("firstInterestPeriod: " + firstInterestPeriod);

        result.setFirstInterestPeriod(firstInterestPeriod);

        if (!securityObject.has("firstInterestPaymentDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name firstInterestPaymentDate");

        Date firstInterestPaymentDate = asDate ("firstInterestPaymentDate", securityObject.get("firstInterestPaymentDate"));

        log.debug("firstInterestPaymentDate: " + firstInterestPaymentDate);

        result.setFirstInterestPaymentDate(firstInterestPaymentDate);

        if (!securityObject.has("floatingRate"))
            throw new ConversionFailedException("The JSON does not contain a member with name floatingRate");

        String floatingRate = securityObject.get("floatingRate").getAsString();

        log.debug("floatingRate: " + floatingRate);

        result.setFloatingRate(floatingRate);

        if (!securityObject.has("frnIndexDeterminationDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name frnIndexDeterminationDate");

        Date frnIndexDeterminationDate = asDate ("frnIndexDeterminationDate", securityObject.get("frnIndexDeterminationDate"));

        log.debug("frnIndexDeterminationDate: " + frnIndexDeterminationDate);

        result.setFrnIndexDeterminationDate(frnIndexDeterminationDate);

        if (!securityObject.has("frnIndexDeterminationRate"))
            throw new ConversionFailedException("The JSON does not contain a member with name frnIndexDeterminationRate");

        BigDecimal frnIndexDeterminationRate = asBigDecimal (securityObject.get("frnIndexDeterminationRate"));//.getAsBigDecimal();

        log.debug("frnIndexDeterminationRate: " + frnIndexDeterminationRate);

        result.setFrnIndexDeterminationRate(frnIndexDeterminationRate);

        if (!securityObject.has("highDiscountRate"))
            throw new ConversionFailedException("The JSON does not contain a member with name highDiscountRate");

        BigDecimal highDiscountRate = asBigDecimal (securityObject.get("highDiscountRate"));//.getAsBigDecimal();

        log.debug("highDiscountRate: " + highDiscountRate);

        result.setHighDiscountRate(highDiscountRate);

        if (!securityObject.has("highInvestmentRate"))
            throw new ConversionFailedException("The JSON does not contain a member with name highInvestmentRate");

        BigDecimal highInvestmentRate = asBigDecimal (securityObject.get("highInvestmentRate"));//.getAsBigDecimal();

        log.debug("highInvestmentRate: " + highInvestmentRate);

        result.setHighInvestmentRate(highInvestmentRate);

        if (!securityObject.has("highPrice"))
            throw new ConversionFailedException("The JSON does not contain a member with name highPrice");

        String highPrice = securityObject.get("highPrice").getAsString();

        log.debug("highPrice: " + highPrice);

        result.setHighPrice(highPrice);

        if (!securityObject.has("highDiscountMargin"))
            throw new ConversionFailedException("The JSON does not contain a member with name highDiscountMargin");

        BigDecimal highDiscountMargin = asBigDecimal (securityObject.get("highDiscountMargin"));//.getAsBigDecimal();

        log.debug("highDiscountMargin: " + highDiscountMargin);

        result.setHighDiscountMargin(highDiscountMargin);

        if (!securityObject.has("highYield"))
            throw new ConversionFailedException("The JSON does not contain a member with name highYield");

        BigDecimal highYield = asBigDecimal (securityObject.get("highYield"));//.getAsBigDecimal();

        log.debug("highYield: " + highYield);

        result.setHighYield(highYield);

        if (!securityObject.has("indexRatioOnIssueDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name indexRatioOnIssueDate");

        BigDecimal indexRatioOnIssueDate = asBigDecimal (securityObject.get("indexRatioOnIssueDate"));//.getAsBigDecimal();

        log.debug("indexRatioOnIssueDate: " + indexRatioOnIssueDate);

        result.setIndexRatioOnIssueDate(indexRatioOnIssueDate);

        if (!securityObject.has("indirectBidderAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name indirectBidderAccepted");

        Long indirectBidderAccepted = asLong (securityObject.get("indirectBidderAccepted"));

        log.debug("indirectBidderAccepted: " + indirectBidderAccepted);

        result.setIndirectBidderAccepted(indirectBidderAccepted);

        if (!securityObject.has("indirectBidderTendered"))
            throw new ConversionFailedException("The JSON does not contain a member with name indirectBidderTendered");

        Long indirectBidderTendered = asLong (securityObject.get("indirectBidderTendered"));

        log.debug("indirectBidderTendered: " + indirectBidderTendered);

        result.setIndirectBidderTendered(indirectBidderTendered);

        if (!securityObject.has("interestPaymentFrequency"))
            throw new ConversionFailedException("The JSON does not contain a member with name interestPaymentFrequency");

        String interestPaymentFrequency = securityObject.get("interestPaymentFrequency").getAsString();

        log.debug("interestPaymentFrequency: " + interestPaymentFrequency);

        result.setInterestPaymentFrequency(interestPaymentFrequency);

        if (!securityObject.has("lowDiscountRate"))
            throw new ConversionFailedException("The JSON does not contain a member with name lowDiscountRate");

        BigDecimal lowDiscountRate = asBigDecimal (securityObject.get("lowDiscountRate"));//.getAsBigDecimal();

        log.debug("lowDiscountRate: " + lowDiscountRate);

        result.setLowDiscountRate(lowDiscountRate);

        if (!securityObject.has("lowInvestmentRate"))
            throw new ConversionFailedException("The JSON does not contain a member with name lowInvestmentRate");

        BigDecimal lowInvestmentRate = asBigDecimal (securityObject.get("lowInvestmentRate"));//.getAsBigDecimal();

        log.debug("lowInvestmentRate: " + lowInvestmentRate);

        result.setLowInvestmentRate(lowInvestmentRate);

        if (!securityObject.has("lowPrice"))
            throw new ConversionFailedException("The JSON does not contain a member with name lowPrice");

        BigDecimal lowPrice = asBigDecimal (securityObject.get("lowPrice"));//.getAsBigDecimal();

        log.debug("lowPrice: " + lowPrice);

        result.setLowPrice(lowPrice);

        if (!securityObject.has("lowDiscountMargin"))
            throw new ConversionFailedException("The JSON does not contain a member with name lowDiscountMargin");

        BigDecimal lowDiscountMargin = asBigDecimal (securityObject.get("lowDiscountMargin"));//.getAsBigDecimal();

        log.debug("lowDiscountMargin: " + lowDiscountMargin);

        result.setLowDiscountMargin(lowDiscountMargin);

        if (!securityObject.has("lowYield"))
            throw new ConversionFailedException("The JSON does not contain a member with name lowYield");

        BigDecimal lowYield = asBigDecimal (securityObject.get("lowYield"));//.getAsBigDecimal();

        log.debug("lowYield: " + lowYield);

        result.setLowYield(lowYield);

        if (!securityObject.has("maturingDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name maturingDate");

        Date maturingDate = asDate ("maturingDate", securityObject.get("maturingDate"));

        log.debug("maturingDate: " + maturingDate);

        result.setMaturingDate(maturingDate);

        if (!securityObject.has("maximumCompetitiveAward"))
            throw new ConversionFailedException("The JSON does not contain a member with name maximumCompetitiveAward");

        BigDecimal maximumCompetitiveAward = asBigDecimal (securityObject.get("maximumCompetitiveAward"));//.getAsBigDecimal();

        log.debug("maximumCompetitiveAward: " + maximumCompetitiveAward);

        result.setMaximumCompetitiveAward(maximumCompetitiveAward);

        if (!securityObject.has("maximumNoncompetitiveAward"))
            throw new ConversionFailedException("The JSON does not contain a member with name maximumNoncompetitiveAward");

        BigDecimal maximumNoncompetitiveAward = asBigDecimal (securityObject.get("maximumNoncompetitiveAward"));//.getAsBigDecimal();

        log.debug("maximumNoncompetitiveAward: " + maximumNoncompetitiveAward);

        result.setMaximumNoncompetitiveAward(maximumNoncompetitiveAward);

        if (!securityObject.has("maximumSingleBid"))
            throw new ConversionFailedException("The JSON does not contain a member with name maximumSingleBid");

        BigDecimal maximumSingleBid = asBigDecimal (securityObject.get("maximumSingleBid"));//.getAsBigDecimal();

        log.debug("maximumSingleBid: " + maximumSingleBid);

        result.setMaximumSingleBid(maximumSingleBid);

        if (!securityObject.has("minimumBidAmount"))
            throw new ConversionFailedException("The JSON does not contain a member with name minimumBidAmount");

        BigDecimal minimumBidAmount = asBigDecimal (securityObject.get("minimumBidAmount"));//.getAsBigDecimal();

        log.debug("minimumBidAmount: " + minimumBidAmount);

        result.setMinimumBidAmount(minimumBidAmount);

        if (!securityObject.has("minimumStripAmount"))
            throw new ConversionFailedException("The JSON does not contain a member with name minimumStripAmount");

        Integer minimumStripAmount = asInteger (securityObject.get("minimumStripAmount"));

        log.debug("minimumStripAmount: " + minimumStripAmount);

        result.setMinimumStripAmount(minimumStripAmount);

        if (!securityObject.has("minimumToIssue"))
            throw new ConversionFailedException("The JSON does not contain a member with name minimumToIssue");

        Integer minimumToIssue = asInteger (securityObject.get("minimumToIssue"));

        log.debug("minimumToIssue: " + minimumToIssue);

        result.setMinimumToIssue(minimumToIssue);

        if (!securityObject.has("multiplesToBid"))
            throw new ConversionFailedException("The JSON does not contain a member with name multiplesToBid");

        Integer multiplesToBid = asInteger (securityObject.get("multiplesToBid"));

        log.debug("multiplesToBid: " + multiplesToBid);

        result.setMultiplesToBid(multiplesToBid);

        if (!securityObject.has("multiplesToIssue"))
            throw new ConversionFailedException("The JSON does not contain a member with name multiplesToIssue");

        Integer multiplesToIssue = asInteger (securityObject.get("multiplesToIssue"));

        log.debug("multiplesToIssue: " + multiplesToIssue);

        result.setMultiplesToIssue(multiplesToIssue);

        if (!securityObject.has("nlpExclusionAmount"))
            throw new ConversionFailedException("The JSON does not contain a member with name nlpExclusionAmount");

        Long nlpExclusionAmount = asLong (securityObject.get("nlpExclusionAmount"));

        log.debug("nlpExclusionAmount: " + nlpExclusionAmount);

        result.setNlpExclusionAmount(nlpExclusionAmount);

        if (!securityObject.has("nlpReportingThreshold"))
            throw new ConversionFailedException("The JSON does not contain a member with name nlpReportingThreshold");

        String nlpReportingThreshold = securityObject.get("nlpReportingThreshold").getAsString();

        log.debug("nlpReportingThreshold: " + nlpReportingThreshold);

        result.setNlpReportingThreshold(nlpReportingThreshold);

        if (!securityObject.has("noncompetitiveAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name noncompetitiveAccepted");

        Long noncompetitiveAccepted = asLong (securityObject.get("noncompetitiveAccepted"));

        log.debug("noncompetitiveAccepted: " + noncompetitiveAccepted);

        result.setNoncompetitiveAccepted(noncompetitiveAccepted);

        if (!securityObject.has("noncompetitiveTendersAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name noncompetitiveTendersAccepted");

        String noncompetitiveTendersAccepted = securityObject.get("noncompetitiveTendersAccepted").getAsString();

        log.debug("noncompetitiveTendersAccepted: " + noncompetitiveTendersAccepted);

        result.setNoncompetitiveTendersAccepted(asBoolean (noncompetitiveTendersAccepted));

        if (!securityObject.has("offeringAmount"))
            throw new ConversionFailedException("The JSON does not contain a member with name offeringAmount");

        Long offeringAmount = asLong (securityObject.get("offeringAmount"));

        log.debug("offeringAmount: " + offeringAmount);

        result.setOfferingAmount(offeringAmount);

        if (!securityObject.has("originalCusip"))
            throw new ConversionFailedException("The JSON does not contain a member with name originalCusip");

        String originalCusip = securityObject.get("originalCusip").getAsString();

        log.debug("originalCusip: " + originalCusip);

        result.setOriginalCusip(originalCusip);

        if (!securityObject.has("originalDatedDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name originalDatedDate");

        // "originalDatedDate":"2017-04-15T00:00:00"
        Date originalDatedDate = asDate ("originalDatedDate", securityObject.get("originalDatedDate"));

        log.debug("originalDatedDate: " + originalDatedDate);

        result.setOriginalDatedDate(originalDatedDate);

        if (!securityObject.has("originalIssueDate"))
            throw new ConversionFailedException("The JSON does not contain a member with name originalIssueDate");

        /*
         * "originalIssueDate":"2013-10-17T00:00:00"
         */
        Date originalIssueDate = asDate ("originalIssueDate", securityObject.get("originalIssueDate"));

        log.debug("originalIssueDate: " + originalIssueDate);

        result.setOriginalIssueDate(originalIssueDate);

        if (!securityObject.has("originalSecurityTerm"))
            throw new ConversionFailedException("The JSON does not contain a member with name originalSecurityTerm");

        String originalSecurityTerm = securityObject.get("originalSecurityTerm").getAsString();

        log.debug("originalSecurityTerm: " + originalSecurityTerm);

        result.setOriginalSecurityTerm(originalSecurityTerm);

        if (!securityObject.has("pdfFilenameAnnouncement"))
            throw new ConversionFailedException("The JSON does not contain a member with name pdfFilenameAnnouncement");

        String pdfFilenameAnnouncement = securityObject.get("pdfFilenameAnnouncement").getAsString();

        log.debug("pdfFilenameAnnouncement: " + pdfFilenameAnnouncement);

        result.setPdfFilenameAnnouncement(pdfFilenameAnnouncement);

        if (!securityObject.has("pdfFilenameCompetitiveResults"))
            throw new ConversionFailedException("The JSON does not contain a member with name pdfFilenameCompetitiveResults");

        String pdfFilenameCompetitiveResults = securityObject.get("pdfFilenameCompetitiveResults").getAsString();

        log.debug("pdfFilenameCompetitiveResults: " + pdfFilenameCompetitiveResults);

        result.setPdfFilenameCompetitiveResults(pdfFilenameCompetitiveResults);

        if (!securityObject.has("pdfFilenameNoncompetitiveResults"))
            throw new ConversionFailedException("The JSON does not contain a member with name pdfFilenameNoncompetitiveResults");

        String pdfFilenameNoncompetitiveResults = securityObject.get("pdfFilenameNoncompetitiveResults").getAsString();

        log.debug("pdfFilenameNoncompetitiveResults: " + pdfFilenameNoncompetitiveResults);

        result.setPdfFilenameNoncompetitiveResults(pdfFilenameNoncompetitiveResults);

        if (!securityObject.has("pdfFilenameSpecialAnnouncement"))
            throw new ConversionFailedException("The JSON does not contain a member with name pdfFilenameSpecialAnnouncement");

        String pdfFilenameSpecialAnnouncement = securityObject.get("pdfFilenameSpecialAnnouncement").getAsString();

        log.debug("pdfFilenameSpecialAnnouncement: " + pdfFilenameSpecialAnnouncement);

        result.setPdfFilenameSpecialAnnouncement(pdfFilenameSpecialAnnouncement);

        if (!securityObject.has("pricePer100"))
            throw new ConversionFailedException("The JSON does not contain a member with name pricePer100");

        BigDecimal pricePer100 = asBigDecimal (securityObject.get("pricePer100"));//.getAsBigDecimal();

        log.debug("pricePer100: " + pricePer100);

        result.setPricePer100(pricePer100);

        if (!securityObject.has("primaryDealerAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name primaryDealerAccepted");

        Long primaryDealerAccepted = asLong (securityObject.get("primaryDealerAccepted"));

        log.debug("primaryDealerAccepted: " + primaryDealerAccepted);

        result.setPrimaryDealerAccepted(primaryDealerAccepted);

        if (!securityObject.has("primaryDealerTendered"))
            throw new ConversionFailedException("The JSON does not contain a member with name primaryDealerTendered");

        Long primaryDealerTendered = asLong (securityObject.get("primaryDealerTendered"));

        log.debug("primaryDealerTendered: " + primaryDealerTendered);

        result.setPrimaryDealerTendered(primaryDealerTendered);

        if (!securityObject.has("reopening"))
            throw new ConversionFailedException("The JSON does not contain a member with name reopening");

        String reopening = securityObject.get("reopening").getAsString();

        log.debug("reopening: " + reopening);

        result.setReopening(asBoolean (reopening));

        if (!securityObject.has("securityTermDayMonth"))
            throw new ConversionFailedException("The JSON does not contain a member with name securityTermDayMonth");

        String securityTermDayMonth = securityObject.get("securityTermDayMonth").getAsString();

        log.debug("securityTermDayMonth: " + securityTermDayMonth);

        result.setSecurityTermDayMonth(securityTermDayMonth);

        if (!securityObject.has("securityTermWeekYear"))
            throw new ConversionFailedException("The JSON does not contain a member with name securityTermWeekYear");

        String securityTermWeekYear = securityObject.get("securityTermWeekYear").getAsString();

        log.debug("securityTermWeekYear: " + securityTermWeekYear);

        result.setSecurityTermWeekYear(securityTermWeekYear);

        if (!securityObject.has("series"))
            throw new ConversionFailedException("The JSON does not contain a member with name series");

        String series = securityObject.get("series").getAsString();

        log.debug("series: " + series);

        result.setSeries(series);

        if (!securityObject.has("somaAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name somaAccepted");

        Long somaAccepted = asLong (securityObject.get("somaAccepted"));

        log.debug("somaAccepted: " + somaAccepted);

        result.setSomaAccepted(somaAccepted);

        if (!securityObject.has("somaHoldings"))
            throw new ConversionFailedException("The JSON does not contain a member with name somaHoldings");

        Long somaHoldings = asLong (securityObject.get("somaHoldings"));

        log.debug("somaHoldings: " + somaHoldings);

        result.setSomaHoldings(somaHoldings);

        if (!securityObject.has("somaIncluded"))
            throw new ConversionFailedException("The JSON does not contain a member with name somaIncluded");

        String somaIncluded = securityObject.get("somaIncluded").getAsString();

        log.debug("somaIncluded: " + somaIncluded);

        result.setSomaIncluded(asBoolean (somaIncluded));

        if (!securityObject.has("somaTendered"))
            throw new ConversionFailedException("The JSON does not contain a member with name somaTendered");

        Long somaTendered = asLong (securityObject.get("somaTendered"));

        log.debug("somaTendered: " + somaTendered);

        result.setSomaTendered(somaTendered);

        if (!securityObject.has("spread"))
            throw new ConversionFailedException("The JSON does not contain a member with name spread");

        BigDecimal spread = asBigDecimal (securityObject.get("spread"));//.getAsBigDecimal();

        log.debug("spread: " + spread);

        result.setSpread(spread);

        if (!securityObject.has("standardInterestPaymentPer1000"))
            throw new ConversionFailedException("The JSON does not contain a member with name standardInterestPaymentPer1000");

        BigDecimal standardInterestPaymentPer1000 = asBigDecimal (securityObject.get("standardInterestPaymentPer1000"));//.getAsBigDecimal();

        log.debug("standardInterestPaymentPer1000: " + standardInterestPaymentPer1000);

        result.setStandardInterestPaymentPer1000(standardInterestPaymentPer1000);

        if (!securityObject.has("strippable"))
            throw new ConversionFailedException("The JSON does not contain a member with name strippable");

        String strippable = securityObject.get("strippable").getAsString();

        log.debug("strippable: " + strippable);

        result.setStrippable(asBoolean (strippable));

        if (!securityObject.has("term"))
            throw new ConversionFailedException("The JSON does not contain a member with name term");

        String term = securityObject.get("term").getAsString();

        log.debug("term: " + term);

        result.setTerm(term);

        if (!securityObject.has("tiinConversionFactorPer1000"))
            throw new ConversionFailedException("The JSON does not contain a member with name tiinConversionFactorPer1000");

        BigDecimal tiinConversionFactorPer1000 = asBigDecimal (securityObject.get("tiinConversionFactorPer1000"));//.getAsBigDecimal();

        log.debug("tiinConversionFactorPer1000: " + tiinConversionFactorPer1000);

        result.setTiinConversionFactorPer1000(tiinConversionFactorPer1000);

        if (!securityObject.has("tips"))
            throw new ConversionFailedException("The JSON does not contain a member with name tips");

        /*
         * "No"
         */
        String tips = securityObject.get("tips").getAsString();

        log.debug("tips: " + tips);

        result.setTips(asBoolean (tips));

        if (!securityObject.has("totalAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name totalAccepted");

        Long totalAccepted = asLong (securityObject.get("totalAccepted"));

        log.debug("totalAccepted: " + totalAccepted);

        result.setTotalAccepted(totalAccepted);

        if (!securityObject.has("totalTendered"))
            throw new ConversionFailedException("The JSON does not contain a member with name totalTendered");

        Long totalTendered = asLong (securityObject.get("totalTendered"));

        log.debug("totalTendered: " + totalTendered);

        result.setTotalTendered(totalTendered);

        if (!securityObject.has("treasuryDirectAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name treasuryDirectAccepted");

        Long treasuryDirectAccepted = asLong (securityObject.get("treasuryDirectAccepted"));

        log.debug("treasuryDirectAccepted: " + treasuryDirectAccepted);

        result.setTreasuryDirectAccepted(treasuryDirectAccepted);

        if (!securityObject.has("treasuryDirectTendersAccepted"))
            throw new ConversionFailedException("The JSON does not contain a member with name treasuryDirectTendersAccepted");

        // "No"
        String treasuryDirectTendersAccepted = securityObject.get("treasuryDirectTendersAccepted").getAsString();

        log.debug("treasuryDirectTendersAccepted: " + treasuryDirectTendersAccepted);

        result.setTreasuryDirectTendersAccepted(asBoolean (treasuryDirectTendersAccepted));

        if (!securityObject.has("type"))
            throw new ConversionFailedException("The JSON does not contain a member with name type");

        String type = securityObject.get("type").getAsString();

        log.debug("type: " + type);

        result.setType(type);

        if (!securityObject.has("unadjustedAccruedInterestPer1000"))
            throw new ConversionFailedException("The JSON does not contain a member with name unadjustedAccruedInterestPer1000");

        BigDecimal unadjustedAccruedInterestPer1000 = asBigDecimal (securityObject.get("unadjustedAccruedInterestPer1000"));//.getAsBigDecimal();

        log.debug("unadjustedAccruedInterestPer1000: " + unadjustedAccruedInterestPer1000);

        result.setUnadjustedAccruedInterestPer1000(unadjustedAccruedInterestPer1000);

        if (!securityObject.has("unadjustedPrice"))
            throw new ConversionFailedException("The JSON does not contain a member with name unadjustedPrice");

        BigDecimal unadjustedPrice = asBigDecimal (securityObject.get("unadjustedPrice"));//.getAsBigDecimal ();

        log.debug("unadjustedPrice: " + unadjustedPrice);

        result.setUnadjustedPrice(unadjustedPrice);

        if (!securityObject.has("updatedTimestamp"))
            throw new ConversionFailedException("The JSON does not contain a member with name updatedTimestamp");

        /*
         * 2014-03-07T12:05:06
         */
        Date updatedTimestamp = asDate ("updatedTimestamp", securityObject.get("updatedTimestamp"));

        log.debug("updatedTimestamp: " + updatedTimestamp);

        result.setUpdatedTimestamp(updatedTimestamp);

        if (!securityObject.has("xmlFilenameAnnouncement"))
            throw new ConversionFailedException("The JSON does not contain a member with name xmlFilenameAnnouncement");

        String xmlFilenameAnnouncement = securityObject.get("xmlFilenameAnnouncement").getAsString();

        log.debug("xmlFilenameAnnouncement: " + xmlFilenameAnnouncement);

        result.setXmlFilenameAnnouncement(xmlFilenameAnnouncement);

        if (!securityObject.has("xmlFilenameCompetitiveResults"))
            throw new ConversionFailedException("The JSON does not contain a member with name xmlFilenameCompetitiveResults");

        String xmlFilenameCompetitiveResults = securityObject.get("xmlFilenameCompetitiveResults").getAsString();

        log.debug("xmlFilenameCompetitiveResults: " + xmlFilenameCompetitiveResults);

        result.setXmlFilenameCompetitiveResults(xmlFilenameCompetitiveResults);

        if (!securityObject.has("xmlFilenameSpecialAnnouncement"))
            throw new ConversionFailedException("The JSON does not contain a member with name xmlFilenameSpecialAnnouncement");

        String xmlFilenameSpecialAnnouncement = securityObject.get("xmlFilenameSpecialAnnouncement").getAsString();

        log.debug("xmlFilenameSpecialAnnouncement: " + xmlFilenameSpecialAnnouncement);

        result.setXmlFilenameSpecialAnnouncement(xmlFilenameSpecialAnnouncement);

        log.debug("asSecurity: method ends; result: " + result);

        return result;
    }

    List<Security> asSecurityList (JsonArray securityArray) {

        List<Security> result = new ArrayList<Security> ();

        securityArray.forEach(
            element -> {

                Security security = asSecurity ((JsonObject) element);

                result.add(security);
            }
        );

        return result;
    }

    @Override
    public void write(JsonWriter writer, Securities securities) throws IOException {
        throw new MethodNotSupportedException ("The write method is not supported.");
    }

    static final String YES = "yes", NO = "no";

    Boolean asBoolean (String yesNo) {
        return (yesNo != null && YES.equals (yesNo)) ? true : false;
    }

    Long asLong (JsonElement element) {

        String text = element.getAsString();

        Long result = (text == null || "".equals(text)) ? null : element.getAsLong ();

        return result;
    }

    /**
     * securityObject.get("accruedInterestPer1000").getAsBigDecimal();
     */
    Integer asInteger (JsonElement element) {

        String text = element.getAsString();

        Integer result = (text == null || "".equals(text)) ? null : element.getAsInt ();

        return result;
    }

    /**
     * securityObject.get("accruedInterestPer1000").getAsBigDecimal();
     */
    BigDecimal asBigDecimal (JsonElement element) {

        String text = element.getAsString();

        BigDecimal result = (text == null || "".equals(text)) ? null : element.getAsBigDecimal ();

        return result;
    }

    Date asTime (String elementName, JsonElement element) {
        return asDate (elementName, element, TIME_ONLY_FORMAT);
    }

    Date asDate (String elementName, JsonElement element) {
        return asDate (elementName, element, DATE_T_TIME_DATE_FORMAT);
    }

    Date asDate (String elementName, JsonElement element, DateFormat dateFormat) {

        String text = element.getAsString();

        try {
            return (text == null || "".equals(text)) ? null : dateFormat.parse(text);
        } catch (ParseException parseException) {
            throw new DateConversionFailedRuntimeException ("Unable to convert the date '" + text + "' to a date " +
                "for element: " + elementName, parseException);
        }
    }
}
