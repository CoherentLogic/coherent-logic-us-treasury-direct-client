package com.coherentlogic.treasurydirect.client.core.exceptions;

import org.springframework.core.NestedRuntimeException;

public class DateConversionFailedRuntimeException extends NestedRuntimeException {

    private static final long serialVersionUID = 1L;

    public DateConversionFailedRuntimeException(String msg) {
        super(msg);
    }

    public DateConversionFailedRuntimeException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
